# ROVE Project

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

### Installation

```sh
$ cd project
$ composer install
$ npm install
```

### Konfigurasi database
- Rename file .env.example -> .env
- Konfigurasi database, nama database, password database

### Konfigurasi lanjutan
```sh
$ php artisan key:generate
$ php artisan migrate:refresh --seed
$ gulp
$ php artisan serve
```