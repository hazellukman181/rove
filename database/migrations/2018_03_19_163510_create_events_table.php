<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('museum_id')->unsigned();
            $table->foreign('museum_id')->references('id')->on('museums');
            $table->string('title');
            $table->string('subtitle');
            $table->string('pamflet');
            $table->text('content');
            $table->integer('likes')->default(0);
            $table->date('date');
            $table->time('time');
            $table->string('cp');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
