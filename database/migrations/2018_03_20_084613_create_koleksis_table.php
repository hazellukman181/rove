<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKoleksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('koleksis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('museum_id')->unsigned();
            $table->foreign('museum_id')->references('id')->on('museums');
            $table->string('qrcode');
            $table->string('id_qrcode');
            $table->string('photo');
            $table->string('title');
            $table->text('story');
            $table->text('story1');
            $table->string('story2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('koleksis');
    }
}
