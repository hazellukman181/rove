<?php

use Illuminate\Database\Seeder;

class KoleksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Koleksi::create([
        	'museum_id' => '1',
        	'title' => 'Kereta kencana kasunanan surakarta',
        	'story' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sem augue, rutrum vitae gravida ut, vehicula id ipsum. Nulla fringilla luctus nibh. Duis vulputate est sed leo dictum pretium. Morbi at egestas dui, vel eleifend neque. Maecenas sem neque, porttitor ornare turpis sit amet, lobortis feugiat nibh. Sed venenatis nunc non augue consequat, sed molestie tellus scelerisque. Curabitur nec ante nec leo finibus vehicula. Praesent id magna venenatis, dignissim dolor id, hendrerit tortor. Morbi lectus felis, pretium sit amet semper fringilla, vehicula id metus. Quisque eget metus lorem. Suspendisse potenti. Vivamus nec sodales lorem, ac sollicitudin enim.',
        	'story1' => 'Curabitur nec ante nec leo finibus vehicula. Praesent id magna venenatis, dignissim dolor id, hendrerit tortor. Morbi lectus felis, pretium sit amet semper fringilla, vehicula id metus. Quisque eget metus lorem. Suspendisse potenti. Vivamus nec sodales lorem, ac sollicitudin enim.',
        	'story2' => 'Jangan pernah melupakan sejarah'
        ]);
    }
}
