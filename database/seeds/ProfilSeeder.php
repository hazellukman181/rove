<?php

use Illuminate\Database\Seeder;

class ProfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Profil::create([
        	'user_id' => '2',
        	'home_address' => 'Gerungsari Timur 28B Tembalang, semarang',
        	'phone_number' => '1234567890',
        	'job' => 'Kurator museum'
        ]);
    }
}
