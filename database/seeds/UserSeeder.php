<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
        	'name' => 'Harum shidiqi',
        	'username' => 'admin',
        	'email' => 'admin@gmail.com',
        	'password' => bcrypt('secret'),
        	'role' => 'admin'
        ]);

        App\User::create([
        	'name' => 'Ahmad bustomi',
        	'username' => 'bustomi',
        	'email' => 'bustomi@gmail.com',
        	'password' => bcrypt('secret'),
        	'role' => 'pengelola'
        ]);
    }
}
