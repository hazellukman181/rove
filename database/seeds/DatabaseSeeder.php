<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ProfilSeeder::class);
        $this->call(MuseumSeeder::class);
        $this->call(EventSeeder::class);
        $this->call(KoleksiSeeder::class);
    }
}
