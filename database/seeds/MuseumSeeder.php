<?php

use Illuminate\Database\Seeder;

class MuseumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Museum::create([
        	'user_id' => '2',
        	'name' => 'Museum Ronggowarsito',
        	'address' => 'Jalan M.T. Hartono 344 Semarang, Jawa Tengah',
        	'phone' => '(024)54638817',
        	'detail' => 'Museum exploring the history & culture of Central Java with various artifacts, models & dioramas.',
            'city' => 'Semarang',
            'price' => '5,000',
            'province' => 'Jawa Tengah',
            'hours' => '09.00-17.00',
        ]);
    }
}
