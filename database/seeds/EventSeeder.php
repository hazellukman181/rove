<?php

use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Event::create([
        	'museum_id' => '1',
        	'title' => 'Night at the Museum',
        	'subtitle' => 'Jelajah museum di malam hari',
        	'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sem augue, rutrum vitae gravida ut, vehicula id ipsum. Nulla fringilla luctus nibh. Duis vulputate est sed leo dictum pretium. Morbi at egestas dui, vel eleifend neque. Maecenas sem neque, porttitor ornare turpis sit amet, lobortis feugiat nibh. Sed venenatis nunc non augue consequat, sed molestie tellus scelerisque. Curabitur nec ante nec leo finibus vehicula. Praesent id magna venenatis, dignissim dolor id, hendrerit tortor. Morbi lectus felis, pretium sit amet semper fringilla, vehicula id metus. Quisque eget metus lorem. Suspendisse potenti. Vivamus nec sodales lorem, ac sollicitudin enim.',
        	'date' => '2017-11-10',
        	'time' => '12:12:12',
            'cp' => 'anton (085740062767)',
            'likes' => '3',
            'status' => 'published',
        ]);

        App\Event::create([
        	'museum_id' => '1',
            'title' => 'Napak tilas sejarah museum',
        	'subtitle' => 'Jangan pernah melupakan sejarah',
        	'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sem augue, rutrum vitae gravida ut, vehicula id ipsum. Nulla fringilla luctus nibh. Duis vulputate est sed leo dictum pretium. Morbi at egestas dui, vel eleifend neque. Maecenas sem neque, porttitor ornare turpis sit amet, lobortis feugiat nibh. Sed venenatis nunc non augue consequat, sed molestie tellus scelerisque. Curabitur nec ante nec leo finibus vehicula. Praesent id magna venenatis, dignissim dolor id, hendrerit tortor. Morbi lectus felis, pretium sit amet semper fringilla, vehicula id metus. Quisque eget metus lorem. Suspendisse potenti. Vivamus nec sodales lorem, ac sollicitudin enim.',
        	'date' => '2018-10-05',
        	'time' => '09:30:00',
            'cp' => 'budi (085745536428)',
            'likes' => '10',
            'status' => 'unpublish'
        ]);
    }
}
