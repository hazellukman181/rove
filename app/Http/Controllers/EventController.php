<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Auth;
use App\User;
use App\Event;
use App\Museum;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:pengelola');
    }

    public function index() {

    	$id = Auth::user()->id;
        $museum = Museum::where('user_id', '=', $id)->select('museums.id')->first();
        if(isset($museum)) {
            $museum_id = $museum->id;
            $events = Event::where('museum_id', '=', $museum_id)->paginate(10);
            return view('data_events.index_event', compact('events'));
        } else {
            return view('data_events.index_event');
        }
    	
    }

    public function publish($id) {
    	$status = "published";
    	$publish_event = Event::where('id', $id)->first();
    	$publish_event->status = $status;
    	$publish_event->save();
    
    	return redirect('event')->with('success', 'Event berhasil di publish');
    }

    public function unpublish($id) {
    	$status = "unpublish";
    	$publish_event = Event::where('id', $id)->first();
    	$publish_event->status = $status;
    	$publish_event->save();
    
    	return redirect('event')->with('delete', 'Event batal di publish');
    }

   	public function create($id) {
    	$museum = Museum::where('user_id', '=', $id)->select('museums.id')->first();
    	$id_museum = $museum->id;
   		return view('data_events.create', compact('id_museum'));
   	}

   	protected function savePhoto(UploadedFile $pamflet)
    {
        $fileName = str_random(10) . '.' . $pamflet->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'image';
        $pamflet->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'image'
        . DIRECTORY_SEPARATOR . $filename;
        return File::delete($path);
    }

   	public function store (Request $request) {
      // dd($request);
   		$this->validate($request, [
            'museum_id' => 'exists:museums,id',
            'title' => 'required',
            'subtitle' => 'required',
            'content' => 'required',
            'date' => 'required',
            'time' => 'required',
            'cp' => 'required',
            'status' => 'required',
            'pamflet' => 'required|mimes:jpeg,jpg,png|max:10240'
        ]);
        $data = $request->only('museum_id', 'title', 'subtitle', 'content', 'date', 'time', 'cp', 'status');

        if ($request->hasFile('pamflet')) {
            $data['pamflet'] = $this->savePhoto($request->file('pamflet'));
        }
        
        $event = Event::create($data);

        return redirect('event')->with('success', 'Event berhasil di tambahkan');
   	}

   	public function edit($id) {
   		$event = Event::find($id);
   		$id = Auth::user()->id;
    	$museum = Museum::where('user_id', '=', $id)->select('museums.id')->first();
    	$id_museum = $museum->id;
   		return view('data_events.edit', compact('event', 'id_museum'));
   	}

		public function update (Request $request, $id) {
      $event = Event::findOrFail($id);
      $this->validate($request, [
            'museum_id' => 'exists:museums,id',
            'title' => 'required',
            'subtitle' => 'required',
            'content' => 'required',
            'date' => 'required',
            'time' => 'required',
            'cp' => 'required',
            'status' => 'required',
            'pamflet' => 'mimes:jpeg,jpg,png|max:10240'
        ]);
        $data = $request->only('museum_id', 'title', 'subtitle', 'content', 'date', 'time', 'cp', 'status');

        if ($request->hasFile('pamflet')) {
            $data['pamflet'] = $this->savePhoto($request->file('pamflet'));
            if($event->pamflet !== '') $this->deletePhoto($event->pamflet);
        }

        $event->update($data);

        return redirect('event')->with('success', 'Event berhasil di perbarui');
   	}

   	public function show($id) {
   		$event = Event::find($id);
   		return view('data_events.show', compact('event'));
   	}

   	public function delete($id) {
   		Event::findOrFail($id)->delete();
      return redirect('event')->with('success', 'Hapus event success');
   	}
}
