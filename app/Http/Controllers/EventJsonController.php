<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Koleksi;
use App\Museum;
use App\Event;

class EventJsonController extends Controller
{
    public function koleksiJson() {
        $koleksi = Koleksi::join('museums', 'museums.id', '=', 'koleksis.museum_id')
                            ->select('museums.name', 'museums.city','koleksis.*')
                            ->get();        
    	return response()->json($koleksi);
    }

    public function museumJson() {
    	$museum = Museum::all();
    	return response()->json($museum);
    }
 
    public function eventJson() {
        $publish = 'published';
        $event = Event::join('museums', 'museums.id', '=', 'events.museum_id')
                        ->where('status', $publish)
                        ->select('museums.name', 'museums.city', 'events.*')
                        ->get();
    	return response()->json($event);
    }
}
