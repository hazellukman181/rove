<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\User;
use App\Museum;
use App\Event;

class ListMuseumController extends Controller
{
    public function listMuseum(Request $request) {
        $query = $request->get('query');
        if (isset($request->query)) {
            $list = Museum::where('name', 'LIKE','%'.$query.'%')->get();
            return view('list_museum.museum', compact('list', 'query'));    
        } else {
            $list = Museum::all();
            return view('list_museum.museum', compact('list'));
        }
    }

    public function listEvent(Request $request) {
    	$newest_event = Event::where('status', '=', 'published')->orderBy('created_at', 'desc')->first();
    	$id_newest_event = Event::where('status', '=', 'published')->orderBy('created_at', 'desc')->select('events.id')->first();
    	$id = $id_newest_event->id;
        
        $query = $request->get('query');
        if (isset($query)) { 
            $events = Event::join('museums', 'museums.id', '=', 'events.museum_id')
                            ->where('status', '=', 'published')
                            ->where('title', 'LIKE','%'.$query.'%')
                            ->orWhere('name', 'LIKE','%'.$query.'%')
                            ->select('museums.*', 'events.*')
                            ->orderBy('events.updated_at', 'desc')
                            ->paginate(10);
            // dd($events);
            return view('list_museum.event', compact('events', 'newest_event', 'id_newest_event', 'query'));
        } else {
            $events = Event::where('status', '=', 'published')->where('id', '!=', $id)->orderBy('updated_at', 'desc')->paginate(9);
            return view('list_museum.event', compact('events', 'newest_event', 'id_newest_event', 'query'));
        } 
        

    }

    public function detailEvent($id) {
    	$detail_event = Event::find($id);
        $suggest_event = Event::where('status', '=', 'published')
                                ->where('id', '!=', $id)
                                ->skip(0)->take(3)->inRandomOrder()->get();
    	return view('list_museum.detail_event', compact('detail_event', 'suggest_event'));
    }

    public function actOnEvent(Request $request, $id)
    {
        $action = $request->get('action');
        dd($action);
        switch ($action) {
            case 'Like':
                Event::where('id', $id)->increment('likes');
                break;
            case 'Unlike':
                Event::where('id', $id)->decrement('likes');
                break;
        }
        return '';
    }
}
