<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Profil;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:pengelola', ['only'=>'indexPengelola']);
        $this->middleware('role:admin', ['only'=>'indexAdmin']);
    }

    public function indexPengelola()
    {    

        $id = Auth::user()->id;
        $user = User::where('id',$id)->first();
        $profil = $user->profil;
        if(!isset($profil)) {
            return view('home', compact('user', 'foto'));
        } else {
            $foto = $profil->photo;
            return view('home', compact('user', 'foto'));
        }

    }

    public function indexAdmin()
    {
        return view('home_admin');
    }
}
