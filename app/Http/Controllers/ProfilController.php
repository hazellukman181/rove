<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Auth;
use App\User;
use App\Profil;
use Hash;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:pengelola');
    }
    
    public function index()
    {
        
        $id = Auth::user()->id;
        $profil = Profil::where('user_id','=',$id)->first();

        return view('profil_pengelola.index', compact('profil', 'foto'));
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = str_random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'image';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'image'
        . DIRECTORY_SEPARATOR . $filename;
        return File::delete($path);
    }

    public function create()
    {
        return view('profil_pengelola.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'user_id' => 'required',
            'home_address' => 'required',
            'phone_number' => 'required',
            'job' => 'required',
            'photo' => 'mimes:jpeg,jpg,png|max:10240'
        ]);
        $data = $request->only('user_id', 'home_address', 'phone_number', 'job');

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
        }

        $profil = Profil::create($data);

        return redirect('profil')->with('success', 'Informasi profil berhasil ditambahkan');
    }

    public function edit($id)
    {
        $profil = Profil::where('user_id', $id)->first();
        return view('profil_pengelola.edit', compact('profil'));
    }

    public function update(Request $request, $id)
    {
        $profil = Profil::findOrFail($id);
        $this->validate($request, [
            'user_id' => 'required',
            'home_address' => 'required',
            'phone_number' => 'required',
            'job' => 'required',
            'photo' => 'mimes:jpeg,jpg,png|max:10240'
        ]);
        $data = $request->only('user_id', 'home_address', 'phone_number', 'job');

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
            if($profil->photo !== '') $this->deletePhoto($profil->photo);
        }

        $profil->update($data);

        return redirect('profil')->with('success', 'Data berhasil diubah');
    }

    public function destroy($id)
    {
        //
    }

    public function updateInfoAkun($id){
        $user = User::findOrFail($id);
        return view('akun.update', compact('user'));
    }

    public function updateInfoPass($id){
        $user = User::findOrFail($id);
        return view('akun.password', compact('user'));
    }

    public function updateInfo(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required|unique:users'
        ]);
        $data = $request->only('name', 'username');

        $user->update($data);

        return redirect('home')->with('success', 'Data berhasil diubah');
    }

    public function updatePass(Request $request, $id){
        $this->validate($request, [
            'password' => 'required|min:6|confirmed',
            ]);
            
        $user = User::findOrFail($id);
        
        $user->fill([
            'password' => Hash::make($request->password),
        ])->save();

        $logout = $request->session()->flush();

        return redirect('login')->with('logout');

    }

}

