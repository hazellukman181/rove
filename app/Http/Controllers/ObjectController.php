<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Response;
use File;
use QrCode;
use Auth;
use Carbon\Carbon;
use Excel;
use PDF;
use App\Koleksi;
use App\Museum;

class ObjectController extends Controller
{
		public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:pengelola');
    }
 
    public function index(Request $request) {
        $user = Auth::user()->get();
        $id = Auth::user()->id;
        $museum = Museum::where('user_id', $id)->select('museums.id')->first();
        // dd($museum);
        if ($museum == null) {
            return view('object.index_koleksi');
        } else {
            $museum = Museum::where('user_id', $id)->select('museums.id')->first();
            $museum_id = $museum->id;
        }
        
        if (isset($museum_id)) {
            
            $q = $request->get('query');         
            $koleksis = Koleksi::where('museum_id', $museum_id)
                                ->where('title', 'LIKE', '%'.$q.'%')
                                ->paginate(10);

            return view('object.index_koleksi', compact('koleksis', 'user', 'q'));

        } else {
            return view('object.index_koleksi');
        }

        
    }

    public function printPdf($id) {
        $data = Koleksi::where('id', $id)->first();
        
        $pic = $data['qrcode'];
        $path = public_path() . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . $pic;
        
        $pdf = PDF::loadView('print.code', compact('data', 'path'));
        $pdf->setPaper('A6', 'portrait');
        return $pdf->stream('invoice.pdf');

        // Excel::create('Data QRcode', function($excel) use($data, $path) {

        // $excel->setTitle('Display QRCode')->setCreator('Rove');

        // $excel->sheet('Data', function($sheet) use($data, $path) 
        // {

        //     $sheet->setOrientation('portrait')
        //         ->setAutoSize(true);

        //     $sheet->loadView('print.qrcode', compact('data', 'path'));

        // });

        // $lastrow = $excel->getActiveSheet()->getHighestRow();
        // $excel->getActiveSheet()->getStyle('A1:Z'.$lastrow)->getAlignment()->setWrapText(true);


        // })->download('pdf');    
    }

    public function printAll() {
        // dd($id);
        $user = Auth::user()->get();
        $id = Auth::user()->id;
        $data_museum = Museum::where('user_id', $id)->first();
        $museum = Museum::where('user_id', $id)->select('museums.id')->first();
        $museum_id = $museum->id;

        $now = Carbon::now();

        $data = Koleksi::where('museum_id', $museum_id)->get();

        // $pic = $data['qrcode'];
        // $path = public_path() . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . $pic;
        
        $pdf = PDF::loadView('print.all_qrcode', compact('data','data_museum','now'));
        $pdf->setPaper('A4', 'portrait');
        return $pdf->stream('invoice.pdf');

        // $pic = $data['qrcode'];
        // $path = public_path() . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . $pic;

        // Excel::create('Data QRcode', function($excel) use($data) {

        // $excel->setTitle('Display QRCode')->setCreator('Rove');

        // $excel->sheet('Data', function($sheet) use($data) 
        // {

        //     $sheet->setOrientation('landscape')
        //         ->setAutoSize(true);

        //     $sheet->loadView('print.all_qrcode', compact('data'));

        // });

        // $lastrow = $excel->getActiveSheet()->getHighestRow();
        // $excel->getActiveSheet()->getStyle('A1:Z'.$lastrow)->getAlignment()->setWrapText(true);


        // })->download('xls');    
    }

    public function create($id) {
    	$museum = Museum::where('user_id', '=', $id)->select('museums.id')->first();
    	$id_museum = $museum->id;
   		return view('object.create', compact('id_museum'));
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = str_random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'image';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'image'
        . DIRECTORY_SEPARATOR . $filename;
        return File::delete($path);
    }

    public function store(Request $request) {
    	$this->validate($request, [
            'museum_id' => 'exists:museums,id',
            'title' => 'required',
            'story' => 'required',
            'id_qrcode' => 'required|max:10|unique:koleksis,id_qrcode',
            'photo' => 'required|mimes:jpeg,jpg,png|max:10240'
        ]);
        $data = $request->only('museum_id', 'title', 'story', 'story1', 'story2');
        
        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
        }
        
        $qrcodename = $request->id_qrcode . '.png';
        $path_qrcode = public_path() . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . $qrcodename;

        QrCode::format('png')->size(200)->generate($request->id_qrcode, $path_qrcode);
       	
       	$data['qrcode'] = $qrcodename;
       	$data['id_qrcode'] = $request->id_qrcode;

        $koleksi = Koleksi::create($data);
        
        return redirect('koleksi')->with('success', 'Koleksi berhasil di tambahkan');
    }

    public function show($id) {
        $koleksi = Koleksi::find($id);
        return view('object.show', compact('koleksi'));
    }

    public function edit($id_koleksi, $id) {
        $koleksi = Koleksi::find($id_koleksi);
        // $id = Auth::user()->id;
        $museum = Museum::where('user_id', '=', $id)->select('museums.id')->first();
        $id_museum = $museum->id;
        return view('object.edit', compact('koleksi', 'id_museum'));
    }

    public function update(Request $request, $id) {
        $koleksi = Koleksi::findOrFail($id);
        $this->validate($request, [
            'museum_id' => 'exists:museums,id',
            'title' => 'required',
            'story' => 'required',
            'photo' => 'mimes:jpeg,jpg,png|max:10240'
        ]);
        $data = $request->only('museum_id', 'title', 'story', 'story1', 'story2');

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
            if($koleksi->photo !== '') $this->deletePhoto($koleksi->photo);
        }
        // dd($data);
        $koleksi->update($data);

        return redirect('koleksi')->with('success', 'Koleksi berhasil di perbarui');
    }

    public function destroy($id) {
        Koleksi::findOrFail($id)->delete();
        return redirect('koleksi')->with('success', 'Hapus koleksi success');
    }

    public function downloadQRCode($id) {
        $koleksi = Koleksi::where('id', $id)->first();
        $qrcode = $koleksi->qrcode;

        $file = public_path() . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . $qrcode;
        $headers = array(
                  'Content-Type: application/png',
                );

        return Response::download($file, $qrcode, $headers);
    }

}
