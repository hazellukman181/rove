<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Museum;
use App\Koleksi;
use PDF;

class PrintController extends Controller
{
    public function print($id) {
    	$museum = Museum::where('user_id', $id)->first();
    	$id_museum = $museum->id;

      $koleksi = Koleksi::where('museum_id', $id_museum)->get();

      $pdf = PDF::loadView('print.koleksi', compact('koleksi'));
      return $pdf->stream('Koleksi Museum.pdf');
    }
}
