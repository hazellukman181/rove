<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Auth;
use App\User;
use App\Museum;
 
class MuseumController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:pengelola');
    }

    public function index() {
        $id = Auth::user()->id;
        $museum = Museum::where('user_id', $id)->first();
        return view('info_museum.index', compact('museum'));
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = str_random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'image';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'image'
        . DIRECTORY_SEPARATOR . $filename;
        return File::delete($path);
    }

    public function create()
    {
        return view('info_museum.create');
    }

    public function store(Request $request)
    {   
        
        $this->validate($request, [
            'user_id' => 'exists:users,id',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'detail' => 'required',
            'city' => 'required',
            'province' => 'required',
            'hours' => 'required',
            'price' => 'required',
            'photo' => 'required|mimes:jpeg,jpg,png|max:10240'
            ]);
        $data = $request->only('name', 'address', 'phone', 'detail', 'city', 'province', 'hours', 'price','user_id');

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
        }

        $museum = Museum::create($data);

        return redirect('museum')->with('success', 'Informasi museum berhasil ditambahkan');
    }

    public function edit($id)
    {
        $museum = Museum::where('user_id', $id)->first();
        return view('info_museum.edit', compact('museum'));
    }

    public function update(Request $request, $id)
    {
        $museum = Museum::findOrFail($id);
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'detail' => 'required',
            'city' => 'required',
            'province' => 'required',
            'hours' => 'required',
            'photo' => 'mimes:jpeg,jpg,png|max:10240'
        ]);
        $data = $request->only('name', 'address', 'phone', 'detail', 'city', 'province', 'hours');

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
            if($museum->photo !== '') $this->deletePhoto($museum->photo);
        }
        $museum->update($data);

        return redirect('museum')->with('success', 'Informasi museum berhasil diperbarui');
    }
}
