<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {return view('welcome');});
Route::get('/homepage', function () {return view('welcome');});

Route::auth();

Route::get('auth/login', function () {return view('auth/login_pengelola');});
Route::get('auth/register/pengelola', function () {return view('auth/register');});
Route::get('auth/login/pengelola', function () {return view('auth/login');});
Route::get('/home', 'HomeController@indexPengelola');
Route::get('/admin', 'HomeController@indexAdmin');
Route::resource('/users', 'UsersController');

// Informasi akun
Route::get('akun/id_pengelola={id}', 'ProfilController@updateInfoAkun');
Route::get('password/id_pengelola={id}', 'ProfilController@updateInfoPass');
Route::patch('update_akun/{id}', 'ProfilController@updateInfo');
Route::get('akun/password_update/id_pengelola={id}', 'ProfilController@passwordUpdate');
Route::patch('update_password/{id}', 'ProfilController@updatePass');

// Profil pengelola museum
Route::get('profil', 'ProfilController@index');
Route::get('/add_profil', 'ProfilController@create');
Route::post('/profil_add', 'ProfilController@store');
Route::get('/profil/{id}', 'ProfilController@edit');
Route::patch('/profil_update/{id}', 'ProfilController@update');

// Data museum
Route::get('museum', 'MuseumController@index');
Route::get('/add_museum', 'MuseumController@create');
Route::post('/museum_add', 'MuseumController@store');
Route::get('/museum/{id}', 'MuseumController@edit');
Route::patch('/museum_update/{id}', 'MuseumController@update');

// Data Event  
Route::get('event', 'EventController@index');
Route::patch('publish_event/{id}', 'EventController@publish');
Route::patch('unpublish_event/{id}', 'EventController@unpublish');
Route::get('add_event/{id}', 'EventController@create');
Route::post('event_add', 'EventController@store');
Route::get('update_event/{id}', 'EventController@edit');
Route::patch('event_update/{id}', 'EventController@update');
Route::get('show_event/{id}', 'EventController@show');
Route::delete('delete_event/{id}', 'EventController@delete');
 
// Data Objek museum
Route::get('koleksi', 'ObjectController@index');
Route::get('add_koleksi/{id}', 'ObjectController@create');
Route::post('koleksi_add', 'ObjectController@store');
Route::get('show_koleksi/{id}', 'ObjectController@show');
Route::get('update_koleksi/{id_koleksi}/{id}', 'ObjectController@edit');
Route::patch('koleksi_update/{id}', 'ObjectController@update');
Route::delete('koleksi_delete/{id}', 'ObjectController@destroy');
Route::get('download_qrcode/{id}', 'ObjectController@downloadQRCode');
Route::get('print/{id}','ObjectController@printPdf');
Route::get('print_all', 'ObjectController@printAll');

// Landing page
Route::get('daftar_museum/', 'ListMuseumController@listMuseum');
Route::get('daftar_event/', 'ListMuseumController@listEvent');
Route::get('detail_event/{id}', 'ListMuseumController@detailEvent');
// Route::post('like/', ['uses'=>'ListMuseumController@actOnEvent', 'as'=>'like']);

Route::get('responsive', function () {return view('responsive_alert');});

// Try json
Route::get('event_json', 'EventJsonController@eventJson');
Route::get('museum_json', 'EventJsonController@museumJson');
Route::get('koleksi_json', 'EventJsonController@koleksiJson');