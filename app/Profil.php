<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $fillable = ['user_id', 'home_address', 'phone_number', 'job', 'photo'];

    public function user() {
    	return $this->belongsTo('App\User');
    }
}
