<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Koleksi extends Model
{
    protected $fillable = ['museum_id', 'id_qrcode','title', 'qrcode', 'story', 'story1', 'story2', 'photo'];

    public function museum() {
    	return $this->belongsTo('App\Museum');
    }
}
