<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Museum extends Model
{
    protected $fillable = ['user_id', 'name', 'price','address', 'phone', 'detail', 'city', 'province', 'hours', 'photo'];

    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function event() {
    	return $this->hasMany('App\Event');
    }

    public function koleksi() {
    	return $this->hasMany('App\Koleksi');
    }
}
