<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        'name', 'email', 'password', 'username',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function museum() {
        return $this->hasOne('App\Museum');
    }

    public function profil() {
        return $this->hasOne('App\Profil');
    }
}
