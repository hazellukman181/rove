<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['museum_id', 'title', 'subtitle', 'pamflet', 'content', 'date', 'time', 'cp', 'status'];

    public function museum() {
    	return $this->belongsTo('App\Museum');
    }
}
