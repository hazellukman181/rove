@extends('layouts.app-admin')

@section('content')
<div class="container-fluid top-admin">
  @include('admin.top')
  @include('alert.alert')
  <div class="row" style="margin-top: 30px;">
    <div class="col-md-6">
      <div class="dash">
        <h1>Dashboard</h1>
        <p class="grey">Pengelolaan data museum dan data koleksi museum</p>
      </div>
      <div class="menu-admin">
        <a href="{{ url('museum') }}"><i class="la la-info-circle la-la fa-margin"></i>Informasi Museum</a>
        <a href="{{ url('koleksi') }}" class="yellowku"><i class="la la-bars la-la fa-margin"></i>Koleksi Museum</a>
        <a href="{{ url('event') }}" class="redku"><i class="la la-paper-plane la-la fa-margin"></i>Event Museum</a>
      </div>
    </div>
    <div class="col-md-6">
      <div class="info">
        <h4 style="display: inline-block; font-weight: bold;">Informasi akun</h4>
        <a href="{{ url('/akun/id_pengelola='.$user->id) }}" style="font-size: 20px; float: right; pointer: cursor;" title="Perbarui data akun" ><i class="la la-gear"></i></a>
        <a href="{{ url('/password/id_pengelola='.$user->id) }}" style="font-size: 20px; float: right; pointer: cursor; margin-right: 10px;" title="Perbarui password" ><i class="la la-lock"></i></a>
        {{-- <a style="display: inline-block; float: right; position:relative; top: 5px;" href="{{ url('/akun/password_update/id_pengelola='.$user->id) }}" class="btn btn-default btn-sm"><i class="la la-unlock"></i> Perbarui password</a> --}}
        <hr>
        <picture>
          @if(isset($foto))
            @if($foto == null)
            <img class="fotone" src="{{ url('../img/placeholder_user.png') }}" alt="">
            @else
            <img src="{{ url('../image/'.$foto) }}" alt="">
            @endif
          @else
            <img class="fotone" src="{{ url('../img/placeholder_user.png') }}" alt="">
          @endif
        </picture>
        <div class="disc">
          {{-- {{ dd($user->profil) }} --}}
          <p class="grey">Nama pengelola</p>
          <h4>{{ $user->name }}</h4>
          <p class="grey">Username</p>
          <h4>{{ $user->username }}</h4>
          <p class="grey">Tanggal mendaftar</p>
          <h4>{{ date('l, d F Y', strtotime($user->created_at)) }}</h4>
          <p class="grey">Alamat email</p>
          <h4>{{ $user->email }}</h4>
          {{-- <a href="{{ url('/akun/id_pengelola='.$user->id) }}" class="btn-log">Perbarui data</a> --}}
        </div>
        <hr>
      </div>
    </div>
  </div>

</div>
{{-- @if (!isset($user->museum))
<div class="row top-line">
  <div class="museum-image2">
    <img src="{{ url('img/logo_admin.png') }}" alt="">
  </div>
  <div class="little-line"></div>
</div>
<div class="row content-profil">
  <div class="col-md-12">
    <div class="row title-info">
      <div class="col-md-6">
        <h2>Informasi Museum</h2>
        <p>Informasi seputar museum berdasarkan data yang diberikan oleh pengelola museum</p>
      </div>
      <div class="col-md-6 text-right">
        <a href="add_museum" class="btn-log" style="position: relative; top: 50px; right: 40px;">Lengkapi Informasi</a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="info-empty">
          <svg class="logoan-empty" xmlns="http://www.w3.org/2000/svg" viewBox="158 1641 96 28.595">
            <defs>
              <style>
                .cls-1 {
                  fill: #c1c1c1;
                }
              </style>
            </defs>
            <g id="logo_white" transform="translate(-30 1596)">
              <path id="Path_5" data-name="Path 5" class="cls-1" d="M18.63,19c3.443-1.62,5.3-4.779,5.3-9.153C23.935,3.564,19.725,0,12.315,0H0S1.06,5.3,6.4,5.3h5.913c3.523,0,5.548,1.58,5.548,4.739,0,3.24-2.021,4.86-5.548,4.86H6.4V9.346S4.3,4.818,0,6.562v16.19a5.6,5.6,0,0,0,5.6,5.6h.8v-8.1H12.92l2.424,4.368a7.25,7.25,0,0,0,6.339,3.732h2.982Z" transform="translate(188 45)"/>
              <path id="Path_6" data-name="Path 6" class="cls-1" d="M336.394,75.52c-7.006,0-11.7,4.415-11.7,11.1,0,6.643,4.7,11.138,11.7,11.138,6.967,0,11.664-4.5,11.664-11.138C348.058,79.935,343.361,75.52,336.394,75.52Zm0,17.173c-3.24,0-5.468-2.43-5.468-5.994s2.228-5.994,5.468-5.994c3.2,0,5.428,2.43,5.428,5.994S339.594,92.693,336.394,92.693Z" transform="translate(-109.351 -24.161)"/>
              <path id="Path_7" data-name="Path 7" class="cls-1" d="M620.9,82.164l-3.742,11.642L613.388,82.18a6.1,6.1,0,0,0-5.845-4.222l-1.973.013,6.494,17.093a7.22,7.22,0,0,0,6.75,4.656h1.419l8.223-21.789h-1.748a6.1,6.1,0,0,0-5.81,4.235Z" transform="translate(-366.581 -26.368)"/>
              <path id="Path_8" data-name="Path 8" class="cls-1" d="M890.509,75.52c-6.643,0-11.219,4.455-11.219,11.179,0,6.6,4.415,11.057,11.381,11.057a12.153,12.153,0,0,0,9.315-3.848l-.492-.5a4.873,4.873,0,0,0-5.33-1.106,7.662,7.662,0,0,1-2.966.589,5.567,5.567,0,0,1-5.63-4.378H901.2C901.728,80.583,898.164,75.52,890.509,75.52Zm-5.063,9.315c.487-2.875,2.349-4.739,5.1-4.739a4.55,4.55,0,0,1,4.82,4.739Z" transform="translate(-617.253 -24.161)"/>
            </g>
          </svg>
          <hr>
          <h3>Data kosong</h3>
          <p>Silahkan lengkapi data museum terlebih dahulu</p>
        </div>
      </div>
    </div>
  </div>
</div>
@else
<div class="row top-line">
  <img class="museum-image" src="{{ url('/image/'.$user->museum->photo) }}" alt="">
  <div class="little-line"></div>
</div>
<div class="row content-profil">
  @include('alert.alert')
  <div class="col-md-12">
    <div class="row title-info">
      <div class="col-md-6">
        <h2>Informasi Museum</h2>
        <p>Informasi seputar museum berdasarkan data yang diberikan oleh pengelola museum</p>
      </div>
      <div class="col-md-6 text-right">
        <a href="museum/{{ $user->id }}" class="btn-log" style="position: relative; top: 50px; right: 40px;">Update Informasi</a>
      </div>
    </div>
    <table class="table-fix table-info table-hovered">
      <thead>
        <tr>
          <td>Informasi</td>
          <td>Konten data</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td width="200px">Nama pengelola</td>
          <td>{{ Auth::user()->name }}</td>
        </tr>
        <tr>
          <td>Nama museum</td>
          <td>{{ $user->museum->name }}</td>
        </tr>
        <tr>
          <td>Alamat museum</td>
          <td>{{ $user->museum->address }}</td>
        </tr>
        <tr>
          <td>Nomor telepon museum</td>
          <td>{{ $user->museum->phone }}</td>
        </tr>
        <tr>
          <td>Tentang museum</td>
          <td>{{ $user->museum->detail }}</td>
        </tr>
        <tr>
          <td>Kota</td>
          <td>{{ $user->museum->city }}</td>
        </tr>
        <tr>
          <td>Provinsi</td>
          <td>{{ $user->museum->province }}</td>
        </tr>
        <tr>
          <td>Jam operasional</td>
          <td>{{ $user->museum->hours }}</td>
        </tr>
        
      </tbody>
    </table>
  </div>
</div>
@endif --}}
@endsection
