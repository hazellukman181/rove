@extends('layouts.app')

@section('content')

{{-- {{dd(request())}} --}}

<div class="container-fluid">
  <div class="side-margin" style="margin: 50px 0;">
    <div class="row" style="margin-bottom: 40px;">
      <div class="col-md-12 text-center middle-titlew">
        <h1>Daftar event</h1>
        <h4 style="margin: 0 auto;">Semua data event di setiap museum akan ditampilkan pada daftar event yang ada dibawah ini</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" style="margin-bottom: 120px;">
        <div class="form-search">
          {!! Form::open(['url'=>['daftar_event'], 'method'=>'get']) !!}
            {!! Form::text('query', null, ['class'=>'form-control input-search']) !!}
            {!! Form::button('<i class="la la-search"></i>', ['type'=>'submit', 'class'=>'btn-search']) !!}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
    @if($query == null)
    <div class="row">
      <div class="col-md-12">
        <div class="title-event">Event Terbaru</div>
      </div>
    </div>
    <div class="row" style="margin-bottom: 40px;">
    	<div class="col-md-6">
        <a href="{{ url('detail_event/'.$newest_event->id) }}" class="black sembro">
        <img class="newest-image" src="{{ url('image/'.$newest_event->pamflet) }}" alt="">
        </a>
      </div>
      <div class="col-md-6">
        <a href="{{ url('detail_event/'.$newest_event->id) }}">
          <div class="title-poster">{{ str_limit($newest_event->title, 50) }}</div>
        </a>
          <div class="title-museum grey"><i class="la la-map-marker grey" style="font-size: 16px; margin-right: 5px;"></i>{{ $newest_event->museum->name }}</div>
          <div class="title-date grey">{{ date('d / m / y', strtotime($newest_event->date)) }}</div>
          <p class="title-content">{{ str_limit($newest_event->content, 300) }}</p>
          <a href="{{ url('detail_event/'.$newest_event->id) }}" class="sembro">Lihat detail <i class="la la-arrow-right"></i></a>
      </div>
    </div>
    @endif
    <div class="row">
      <div class="col-md-12">
        <div class="title-event">Event Museum</div>
      </div>
    </div>
    <div class="row">
    @forelse($events as $museum)
      <div class="col-md-12 container-common">
        <div class="col-md-5">
          <a href="{{ url('detail_event/'.$museum->id) }}" class="black sembro">
            <div class="title-common">{{ str_limit($museum->title, 70) }}</div>
          </a>
          <p>{{ str_limit($museum->content, 120) }}</p>
          <div class="title-place grey"><i class="la la-map-marker" style="font-size: 14px; margin-right: 5px;"></i>{{ $museum->museum->name }}</div>
        </div>
        <div class="col-md-2">
          <a href="{{ url('detail_event/'.$museum->id) }}" class="black sembro">
          <img class="image-poster" src="{{ url('../image/'.$museum->pamflet) }}" alt="">
          </a>
        </div>
        <div class="col-md-5">
          <span class="grey">Tanggal pelaksanaan</span>
          <p class="papo"><i class="la la-calendar grey" style="font-size: 16px; margin-right: 5px;"></i>{{ date('d / m / y', strtotime($museum->date)) }}</p>
          <span class="grey">Waktu event</span>
          <p class="papo"><i class="la la-clock-o grey" style="font-size: 16px; margin-right: 5px;"></i>{{ date('H:m', strtotime($museum->time)) }} WIB</p>
        </div>
      </div>
    @empty
      <div class="row">
        <div class="col-md-12 text-center">
          <img src="{{ url('../img/empty02.png') }}" width="250px" alt="">
          <h1 style="font-weight: bold; margin-bottom: 20px;">Oops</h1>
          <h4 class="grey" style="margin-bottom: 150px;">Pencarian <b>"{{ $query }}"</b> tidak ditemukan</h4>
        </div>
      </div>
    @endforelse
    </div>
    <div class="text-center">
    	{{ $events->links() }}
    </div>
  </div>
</div>

<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="padding: 20px 40px;">
    </div>
  </div>
</div>

@endsection
