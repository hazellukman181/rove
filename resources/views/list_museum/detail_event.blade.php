@extends('layouts.app')

@section('content')

{{-- {{dd(request())}} --}}

<div class="container-fluid">
  <div class="side-margin" style="margin: 50px 0;">
    <div class="row" style="margin-bottom: 120px;">
      <div class="col-md-12 text-center middle-titlew">
        <h1>Detail event</h1>
        <h4 style="margin: 0 auto;">Semua detail event di museum dapat dilihat di daftar berikut</h4>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-6">
        <img src="{{ url('image/'.$detail_event->pamflet) }}" style="width: 100%;" alt="">
      </div>
      <div class="col-md-6" style="padding-left: 50px;">
        <div class="row">
          <div class="col-md-12">
            <div class="title-event">Event Museum</div>
          </div>
        </div>
        
        <h1 style="font-weight: bold;">{{$detail_event->title}}</h1>
        <picture style="display: inline-block;margin-right: 10px; width: 40px;height: 40px;vertical-align: top; margin-top: 20px;">
          @if($detail_event->museum->user->profil == null)
          <img class="pengelola-museum" src="{{ url('../img/placeholder_user.png') }}" alt="">
          @else
          <img class="pengelola-museum" src="{{ url('image/'.$detail_event->museum->user->profil->photo) }}" alt="">
          @endif
        </picture>
        <div style="box-sizing: border-box; display: inline-block; vertical-align: top; margin-top: 20px;">
          <span style="font-size: 12px; line-height: 2;">{{$detail_event->museum->user->name}}</span>
          <p style="font-size: 12px; line-height: 1; color: #999; vertical-align: top;">{{ date('d M Y', strtotime($detail_event->created_at)) }} | {{ date('h:m', strtotime($detail_event->created_at)) }} WIB</p>
        </div>
        
        {{--  <div class="row">
            <a href="#" class="like">Like</a>
            <span id="likes-count-{{ $detail_event->id }}">{{ $detail_event->likes }}</span>
        </div>  --}}
        <hr>
        
        <small style="margin-bottom: 10px"><i class="la la-calendar-o"></i> Waktu pelaksanaan</small>
        <table class="tableo" style="margin-left: -10px;">
          <tbody>
            <tr>
              <td>Tanggal</td>
              <td>{{ date('l, d F Y', strtotime($detail_event->date)) }}</td>
            </tr>
            <tr>
              <td>Waktu</td>
              <td>{{ date('h.m', strtotime($detail_event->time)) }} WIB</td>
            </tr>
            <tr>
              <td>Tempat</td>
              <td>{{ $detail_event->museum->name }}</td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td>{{ $detail_event->museum->address }}</td>
            </tr>
            <tr>
              <td>Kota</td>
              <td>{{ $detail_event->museum->city }}</td>
            </tr>
          </tbody>
        </table>
        <hr>
        <small style="margin-bottom: 10px"><i class="la la-phone-square"></i>Narahubung</small>
        {{ $detail_event->cp }}
      </div>
    </div>

    <div class="row" style="margin-top: 50px;">
      <div class="col-md-6">
        <div class="title-event">Deskripsi Event</div>
      </div>
      <div class="col-md-6" style="padding-left: 80px;">
        <div class="title-event">Event Lainnya</div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <p style="font-size: 20px; opacity: .5; font-style: italic; margin-bottom: 20px;">"{{$detail_event->subtitle}}"</p>
        <p style="line-height: 2.0; margin-bottom: 50px;">{{$detail_event->content}}</p>
      </div>
      <div class="col-md-6" style="padding-left: 80px;">
        @foreach($suggest_event as $event)
          <div class="bawang">
            <a href="{{ url('detail_event/'.$event->id) }}">
              <h4 class="black sembro">{{ str_limit($event->title, 60) }}</h4>
            </a>
            <p class="black">{{ str_limit($event->content, 100) }}</p>
            <span class="s-left grey">{{ $event->museum->name }}</span>
            <span class="s-right grey">{{ date('d / m / y', strtotime($event->date)) }}</span>
          </div>
        @endforeach
      </div>
    </div>

  </div>
</div>


<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="padding: 20px 40px;">
    </div>
  </div>
</div>

@endsection
