@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="side-margin" style="margin: 50px 0;">
    <div class="row" style="margin-bottom:40px;">
      <div class="col-md-12 text-center middle-titlew">
        <h1>Daftar museum</h1>
        <h4 style="margin: 0 auto;">Semua museum yang telah bekerjasama dengan aplikasi Rove dapat dilihat pada daftar berikut</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" style="margin-bottom: 120px;">
        <div class="form-search">
          {!! Form::open(['url'=>['daftar_museum'], 'method'=>'get']) !!}
            {!! Form::text('query', null, ['class'=>'form-control input-search']) !!}
            {!! Form::button('<i class="la la-search"></i>', ['type'=>'submit', 'class'=>'btn-search']) !!}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
    @forelse($list as $museum)
    <div class="row">
      <div class="col-md-12" style="margin-bottom: 50px; border-bottom: 1px solid #ddd; padding-bottom: 50px">
        <div class="col-md-4">
          <img style="width: 100%; height: 200px; object-fit: cover; border-radius: 10px;" src="{{ url('../image/'.$museum->photo) }}" alt="">
        </div>
        <div class="col-md-8">
          <h3 style="margin-top: 5px;">{{ $museum->name }}</h3>
          <p>{{ $museum->address }}</p>
          <div class="little-mid" style="margin-bottom: 15px;">
            <span><i class="fa fa-clock-o" style="color: #ff655c;"></i> {{ $museum->hours }}</span>
            <span><i class="fa fa-phone-square" style="color: #ffc797;"></i> {{ $museum->phone }}</span>
          </div>
          <small style="margin-bottom: 10px;">Deskripsi museum</small>
          <p style="line-height: 1.8">{{ $museum->detail }}</p>
        </div>
      </div>
    </div>
    @empty
    <div class="row">
      <div class="col-md-12 text-center">
        <img src="{{ url('../img/empty02.png') }}" width="250px" alt="">
        <h1 style="font-weight: bold; margin-bottom: 20px;">Oops</h1>
        <h4 class="grey" style="margin-bottom: 150px;">Pencarian <b>"{{ $query }}"</b> tidak ditemukan</h4>
      </div>
    </div>
    @endforelse
    <div class="text-center">
      {{-- {{ $list->links() }} --}}
    </div>
  </div>
</div>

<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="padding: 20px 40px;">
    </div>
  </div>
</div>

@endsection
