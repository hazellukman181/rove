<div class="row">
  <div class="col-md-6">
    <div class="text-left">
      Administrator Pengelola Museum
    </div>
  </div>
  <div class="col-md-6 text-right name-admin">
    <span>{{ Auth::user()->name }}</span>
    @if (isset(Auth::user()->profil))
      @if(Auth::user()->profil->photo)  
        <img class="fotone" src="{{ url('../image/'.Auth::user()->profil->photo) }}" alt="">
      @else
        <img class="fotone" src="{{ url('../img/placeholder_user.png') }}" alt="">
      @endif
    @else
      <img class="fotone" src="{{ url('../img/placeholder_user.png') }}" alt="">
    @endif
  </div>
</div>