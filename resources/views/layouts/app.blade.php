<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{{ url('/img/rove_logo.png') }}}">
    <title>Rove</title>

    {{-- <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet"> --}}
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body id="app-layout">
<div class="show-sm">
  <img src="{{ url('../img/logo_admin.png') }}" alt="">
  <h1>OUR MOBILE SITE COMING SOON! </h1>
  <p>FOR NOW ROVE ONLY AVAILABLE ON DESKTOP :)</p>
</div>
<div class="hidden-md hidden-sm hidden-xs">
  <div class="container-fluid">
  <div class="side-margin">
    <div class="row nav2">
      <div class="col-md-6" style="margin: 40px 0;">
        <a href="{{ url('homepage') }}" class="container-logo">
          <img src="{{ url('../img/main_logo2.svg') }}" class="logo" alt="">
        </a>
      </div>
      <div class="col-md-6" style="margin: 40px 0;">
        @if(Auth::check())
          @can('pengelola-access')
            <div class="right-nav">
              <a href="{{ url('daftar_museum') }}">Museum</a>
              <a href="{{ url('daftar_event') }}">Event museum</a>
              <a href="{{url('/home')}}">{{ Auth::user()->name }}</a>
            </div>
          @endcan
        @endif 

        @if(Auth::guest())
          <div class="right-nav">
            <a href="{{ url('daftar_museum') }}" class="{{ url('daftar_museum') == request()->url() ? 'orange' : '' }}">Museum</a>
            <a href="{{ url('daftar_event') }}" class="{{ url('daftar_event') == request()->url() ? 'orange' : '' }}">Event museum</a>
            <a href="{{ url('auth/register/pengelola') }}" class="{{ url('auth/register/pengelola') == request()->url() ? 'orange' : '' }}">Register</a>
            <a href="{{ url('login') }}" class="{{ url('login') == request()->url() ? 'orange' : '' }}">Login</a>
          </div>
        @endif
      </div>
    </div>

  </div>
</div>

    @yield('content')

<div class="container-fluid">
  <div class="row footero">
    <div class="col-md-4">
      <img src="{{ url('../img/logo_full.png') }}" class="footer-logo" alt="">
    </div>
    <div class="col-md-8 footer-right">
      <a href="{{ url('daftar_museum') }}">Daftar museum</a>
      <a href="{{ url('daftar_event') }}">Event museum</a>
      <a href="{{ url('auth/register/pengelola') }}">Pendaftaran pengelola</a>
    </div>
  </div>
</div>
<div class="footer">
  <div class="text">
    <p class="footer-text">Dikembangkan dengan <i class="la la-heart" style="color: deeppink"></i> dari Semarang untuk museum di indonesia</p>
    <p class="text-right"><i class="la la-copyright"></i> ROVE. 2018 - All Rights Reserved</p>
  </div>
</div>
<div class="foot"></div>
  
</div>
    <script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>
