<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{{ url('/img/rove_logo.png') }}}">
    <title>Rove</title>

    <!-- Styles -->
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/css/style.css')}}">
</head>
<body id="app-layout">
<div class="show-sm">
  <img src="{{ url('../img/logo_admin.png') }}" alt="">
  <h1>OUR MOBILE SITE COMING SOON! </h1>
  <p>FOR NOW ROVE ONLY AVAILABLE ON DESKTOP :)</p>
</div>
  <div class="container-admin hidden-xs hidden-sm hidden-md">
    <div class="panel-left">
      <div class="panel-top">
        <svg class="logoan" xmlns="http://www.w3.org/2000/svg" viewBox="158 1641 96 28.595">
          <defs>
            <style>
              .cls-2 {
                fill: #ffffff !important;
              }
            </style>
          </defs>
          <g id="logo_white" transform="translate(-30 1596)">
            <path id="Path_5" data-name="Path 5" class="cls-2" d="M18.63,19c3.443-1.62,5.3-4.779,5.3-9.153C23.935,3.564,19.725,0,12.315,0H0S1.06,5.3,6.4,5.3h5.913c3.523,0,5.548,1.58,5.548,4.739,0,3.24-2.021,4.86-5.548,4.86H6.4V9.346S4.3,4.818,0,6.562v16.19a5.6,5.6,0,0,0,5.6,5.6h.8v-8.1H12.92l2.424,4.368a7.25,7.25,0,0,0,6.339,3.732h2.982Z" transform="translate(188 45)"/>
            <path id="Path_6" data-name="Path 6" class="cls-2" d="M336.394,75.52c-7.006,0-11.7,4.415-11.7,11.1,0,6.643,4.7,11.138,11.7,11.138,6.967,0,11.664-4.5,11.664-11.138C348.058,79.935,343.361,75.52,336.394,75.52Zm0,17.173c-3.24,0-5.468-2.43-5.468-5.994s2.228-5.994,5.468-5.994c3.2,0,5.428,2.43,5.428,5.994S339.594,92.693,336.394,92.693Z" transform="translate(-109.351 -24.161)"/>
            <path id="Path_7" data-name="Path 7" class="cls-2" d="M620.9,82.164l-3.742,11.642L613.388,82.18a6.1,6.1,0,0,0-5.845-4.222l-1.973.013,6.494,17.093a7.22,7.22,0,0,0,6.75,4.656h1.419l8.223-21.789h-1.748a6.1,6.1,0,0,0-5.81,4.235Z" transform="translate(-366.581 -26.368)"/>
            <path id="Path_8" data-name="Path 8" class="cls-2" d="M890.509,75.52c-6.643,0-11.219,4.455-11.219,11.179,0,6.6,4.415,11.057,11.381,11.057a12.153,12.153,0,0,0,9.315-3.848l-.492-.5a4.873,4.873,0,0,0-5.33-1.106,7.662,7.662,0,0,1-2.966.589,5.567,5.567,0,0,1-5.63-4.378H901.2C901.728,80.583,898.164,75.52,890.509,75.52Zm-5.063,9.315c.487-2.875,2.349-4.739,5.1-4.739a4.55,4.55,0,0,1,4.82,4.739Z" transform="translate(-617.253 -24.161)"/>
          </g>
        </svg>
      </div>

      @if(Auth::check())
        @can('pengelola-access')
          <div class="panel-middle">
            <span>KONFIGURASI AKUN</span>
            <a href="{{ url('home') }}" class="{{ url('home') == request()->url() ? 'aactive' : '' }}"><i class="la la-home fa-margin"></i>Dashboard</a>
            <a href="{{ url('profil') }}" class="{{ url('profil') == request()->url() ? 'aactive' : '' }}"><i class="la la-user fa-margin"></i>Informasi pribadi</a>
            <hr>
            <span>DATA MUSEUM</span>
            <a href="{{ url('museum') }}" class="{{ url('museum') == request()->url() ? 'aactive' : '' }}"><i class="la la-info-circle fa-margin"></i>Informasi museum</a>
            {{-- <a href="{{ url('home') }}" class="{{ url('fhome') == request()->url() ? 'aactive' : '' }}"><i class="la la-plus-circle fa-margin"></i>Update informasi</a> --}}
            <hr>
            <span>INFORMASI MUSEUM</span>
            <a href="{{url('koleksi')}}" class="{{ url('koleksi') == request()->url() ? 'aactive' : '' }}"><i class="la la-bars fa-margin"></i>Koleksi museum</a>
            <a href="{{url('event')}}" class="{{ url('event') == request()->url() ? 'aactive' : '' }}"><i class="la la-paper-plane fa-margin"></i>Event museum</a>
            <hr>
            <a href="{{ url('homepage') }}"><i class="la la-globe fa-margin"></i>Lihat website</a>
            <a href="{{'/logout'}}"><i class="la la-power-off fa-margin"></i>Logout</a>
          </div>
        @endcan
      @endif
    </div>
    
    <div class="panel-right">
        @yield('content')
    </div>
  </div>
  <!-- JavaScripts -->
  <script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>
