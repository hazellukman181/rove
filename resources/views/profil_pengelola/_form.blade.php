<div class="form-group {!! $errors->has('photo') ? 'has-error' : '' !!}">
	{!! Form::label('photo', 'Foto profil') !!}
	@if(isset($model) && $model->photo !== '')
		<div class="row">
			<div class="col-md-6">
				{{-- <p>Current picture</p> --}}
				<div class="thumbnail">
					<img src="{{ url('/image/'.$model->photo) }}" class="img-rounded">
				</div>
			</div>
		</div>
	@endif
	{!! Form::file('photo', ['title'=>'Upload display gambar harus file gambar']) !!}
	<small>Upload foto profil pengelola museum</small>
	{!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('home_address') ? 'has-error' : '' !!}">
	{!! Form::label('home_address', 'Alamat rumah') !!}
	{!! Form::text('home_address', null, ['class'=>'form-control']) !!}
	<small>Isi dengan data alamat rumah pengelola</small>
	{!! $errors->first('home_address', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('phone_number') ? 'has-error' : '' !!}" style="width: 300px;">
	{!! Form::label('phone_number', 'Nomor telepon') !!}
	{!! Form::number('phone_number', null, ['class'=>'form-control']) !!}
	<small>Isi dengan nomor telepon pengelola museum</small>
	{!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('job') ? 'has-error' : '' !!}" style="width: 300px;">
	{!! Form::label('job', 'Jabatan') !!}
	{!! Form::text('job', null, ['class'=>'form-control']) !!}
	<small>Isi dengan jabatan di museum</small>
	{!! $errors->first('job', '<p class="help-block">:message</p>') !!}
</div>


{!! Form::hidden('user_id', Auth::user()->id) !!}
<hr>
{!! Form::submit(isset($model) ? 'Perbarui data' : 'Simpan', ['class'=>'btn-log'] ) !!}
<a href="{{ url('profil') }}" class="btn-log-back">Kembali</a>
