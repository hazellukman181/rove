@extends('layouts.app-admin')

@section('content')
<div class="container-fluid top-admin">
  @include('admin.top')

  <div class="row" style="margin-top: 30px;">
    @include('alert.alert')
    @if(isset($profil))
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="dash" style="margin-bottom: 20px;">
            <h1>Informasi Pribadi</h1>
            <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
          </div>
        </div>
        <div class="col-md-6 text-right">
          <a href="profil/{{ Auth::user()->id }}" class="btn-log" style="position: relative; top: 70px; right: 0px;">Update Informasi</a>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="alert-red"><i class="la la-info-circle la-red"></i><span>Kelola setiap data museum dengan data terbaru</span></div>
        </div>
      </div>
      <hr>
      <div class="row"> 
        <div class="col-md-2">
          @if($profil->photo == null)
          <img style="width: 100%; padding: 3px; border: 1px solid #ddd; border-radius: 5px;" src="{{ url('../img/placeholder_user.png') }}" alt="">
          @else
          <img class="pengelola-profil" src="{{ url('image/'.$profil->photo) }}" alt="">
          @endif
        </div>
        <div class="col-md-10">
          <div class="disc" style="margin-top: 13px;">
            <p class="grey">Nama pengelola</p>
            <h4>{{ $profil->user->name }}</h4>
            <p class="grey">Tanggal mendaftar</p>
            <h4>{{ date('l, d F Y', strtotime($profil->user->created_at)) }}</h4>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <table class="table-fix table-info table-hovered">
            <thead>
              <tr>
                <td>Informasi</td>
                <td>Konten data</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Alamat rumah</td>
                <td>{{ $profil->home_address }}</td>
              </tr>
              <tr>
                <td>Nomor telepon</td>
                <td>{{ $profil->phone_number }}</td>
              </tr>
              <tr>
                <td>Jabatan</td>
                <td>{{ $profil->job }}</td>
              </tr>          
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @else
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="dash" style="margin-bottom: 20px;">
            <h1>Informasi Pribadi</h1>
            <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
          </div>
        </div>
        <div class="col-md-6 text-right">
          <a href="add_profil" class="btn-log" style="position: relative; top: 70px; right: 0px;">Tambah Data</a>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12 text-center empty-state">
          <picture>
            <img src="{{ url('../img/empty02.png') }}" alt="">
            <p class="grey">Data ini masih kosong</p>
            <span class="grey">Silahkan tambahkan data terlebih dahulu untuk dapat melihat semua data</span>
          </picture>
        </div>
      </div>
    </div>
    @endif
  </div>

</div>
@endsection