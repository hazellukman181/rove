@extends('layouts.app-admin')

@section('content')

<div class="container-fluid top-admin">
  @include('admin.top')
  
  <div class="row" style="margin-top: 30px;">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <div class="dash" style="margin-bottom: 20px;">
            <span><a href="{{ url('profil') }}">Informasi profil</a></span> <i class="la la-angle-right"></i> <span>Update Informasi Profil</span>
            <hr>
            <h1>Update Informasi Profil</h1>
            <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
          </div>
        </div>
      </div>      
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="alert-red"><i class="la la-info-circle la-red"></i><span>Kelola setiap data museum dengan data terbaru</span></div>
      <hr>
    </div>
  </div>
  <div class="row">
    <div class="col-md-8">
      {!! Form::model($profil, ['url' => ['profil_update', $profil], 'method' => 'patch', 'files' => true]) !!}
        @include('profil_pengelola._form', ['model' => $profil])
      {!! Form::close() !!}
    </div>
  </div>

</div>

@endsection