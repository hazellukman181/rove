@extends('layouts.app-admin')

@section('content')
<div class="container-fluid top-admin">
  @include('admin.top')

  <div class="row" style="margin-top: 30px;">
    <div class="col-md-6">
      <div class="dash" style="margin-bottom: 20px;">
        <span><a href="{{ url('event') }}">Informasi event</a></span> <i class="la la-angle-right"></i> <span>Tambah Data Event</span>
        <hr>
        <h1>Tambah Event</h1>
        <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
      </div> 
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="alert-red"><i class="la la-info-circle la-red"></i><span>Kelola setiap data museum dengan data terbaru</span></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-8">
      <hr>
      {!! Form::open(['url' => ['event_add'], 'method' => 'post', 'files' => true]) !!}
        @include('data_events._forms')
      {!! Form::close() !!}
    </div>
  </div>

</div>

@endsection