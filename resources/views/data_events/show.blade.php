@extends('layouts.app-admin')

@section('content')

<div class="container-fluid top-admin">
  @include('admin.top')

  <div class="row" style="margin-top: 30px;">
    <div class="col-md-6">
      <div class="dash" style="margin-bottom: 20px;">
        <span><a href="{{ url('event') }}">Informasi event</a></span> <i class="la la-angle-right"></i> <span>Ubah Data Event</span>
        <hr>
        <h1>Detail Informasi Event</h1>
        <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
      </div> 
    </div>
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <br>
      <div class="row">
        <div class="col-md-5">
          @if($event->pamflet !== '')
          <img src="{{ url('image/'.$event->pamflet) }}" class="pengelola-profil" alt="">
          @else
          <div class="info-empty">
            <svg class="logoan-empty" style="width: 40%; padding: 200px 0;" xmlns="http://www.w3.org/2000/svg" viewBox="158 1641 96 28.595">
              <defs>
                <style>
                  .cls-1 {
                    fill: #c1c1c1;
                  }
                </style>
              </defs>
              <g id="logo_white" transform="translate(-30 1596)">
                <path id="Path_5" data-name="Path 5" class="cls-1" d="M18.63,19c3.443-1.62,5.3-4.779,5.3-9.153C23.935,3.564,19.725,0,12.315,0H0S1.06,5.3,6.4,5.3h5.913c3.523,0,5.548,1.58,5.548,4.739,0,3.24-2.021,4.86-5.548,4.86H6.4V9.346S4.3,4.818,0,6.562v16.19a5.6,5.6,0,0,0,5.6,5.6h.8v-8.1H12.92l2.424,4.368a7.25,7.25,0,0,0,6.339,3.732h2.982Z" transform="translate(188 45)"/>
                <path id="Path_6" data-name="Path 6" class="cls-1" d="M336.394,75.52c-7.006,0-11.7,4.415-11.7,11.1,0,6.643,4.7,11.138,11.7,11.138,6.967,0,11.664-4.5,11.664-11.138C348.058,79.935,343.361,75.52,336.394,75.52Zm0,17.173c-3.24,0-5.468-2.43-5.468-5.994s2.228-5.994,5.468-5.994c3.2,0,5.428,2.43,5.428,5.994S339.594,92.693,336.394,92.693Z" transform="translate(-109.351 -24.161)"/>
                <path id="Path_7" data-name="Path 7" class="cls-1" d="M620.9,82.164l-3.742,11.642L613.388,82.18a6.1,6.1,0,0,0-5.845-4.222l-1.973.013,6.494,17.093a7.22,7.22,0,0,0,6.75,4.656h1.419l8.223-21.789h-1.748a6.1,6.1,0,0,0-5.81,4.235Z" transform="translate(-366.581 -26.368)"/>
                <path id="Path_8" data-name="Path 8" class="cls-1" d="M890.509,75.52c-6.643,0-11.219,4.455-11.219,11.179,0,6.6,4.415,11.057,11.381,11.057a12.153,12.153,0,0,0,9.315-3.848l-.492-.5a4.873,4.873,0,0,0-5.33-1.106,7.662,7.662,0,0,1-2.966.589,5.567,5.567,0,0,1-5.63-4.378H901.2C901.728,80.583,898.164,75.52,890.509,75.52Zm-5.063,9.315c.487-2.875,2.349-4.739,5.1-4.739a4.55,4.55,0,0,1,4.82,4.739Z" transform="translate(-617.253 -24.161)"/>
              </g>
            </svg>
            <p>Poster belum di upload</p>
          </div>
          @endif
        </div>
        <div class="col-md-7">
          @if($event->status != "published")
            <span>Status event <span class="btn btn-warning btn-sm" style="cursor: default !importance;">Unpublished</span></span>
          @else
            <span>Status event <span class="btn btn-success btn-sm" style="cursor: default !importance;">Published</span></span>
          @endif

          @if($event->status != "published")
            <a href="{{ url('update_event', $event->id) }}" class="btn btn-danger btn-sm" style="float: right;">Update event</a> 
          @endif
          <hr>
          <h2 style="font-weight: bold; margin-bottom: 20px;">{{$event->title}}</h2>
          <p style="color: #b3b3b3;">{{$event->subtitle}}</p>
          <hr>
          <small style="margin-bottom: 10px"><i class="la la-bars fa-margin"></i>Deskripsi event</small>
          <p style="line-height: 1.9">{{$event->content}}</p>
          <hr>
          <small style="margin-bottom: 10px"><i class="la la-calendar-o fa-margin"></i>Waktu event</small>
          <table class="tableo">
            <tbody>
              <tr>
                <td>Tanggal</td>
                <td>{{ date('l, d F Y', strtotime($event->date)) }}</td>
              </tr>
              <tr>
                <td>Waktu</td>
                <td>{{ date('h.m', strtotime($event->time)) }} WIB</td>
              </tr>
              <tr>
                <td>Tempat</td>
                <td>{{ $event->museum->name }}</td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td>{{ $event->museum->address }}</td>
              </tr>
              <tr>
                <td>Kota</td>
                <td>{{ $event->museum->city }}</td>
              </tr>
            </tbody>
          </table>
          <hr>
          <small style="margin-bottom: 10px"><i class="la la-phone-square fa-margin"></i>Narahubung</small>
          {{ $event->cp }}
        </div>
      </div>
    </div>
  </div>

</div>

@endsection