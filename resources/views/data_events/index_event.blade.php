@extends('layouts.app-admin')

@section('content')

<div class="container-fluid top-admin">
  @include('admin.top')
  
  <div class="row" style="margin-top: 30px;">
    @include('alert.alert')
    @if( isset($events))
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="dash" style="margin-bottom: 20px;">
            <h1>Informasi Event</h1>
            <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
          </div>
        </div>
        <div class="col-md-6 text-right">
          <a href="add_event/{{ Auth::user()->id }}" class="btn-log" style="position: relative; top: 70px; right: 0px;">Tambah Event</a>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="alert-red"><i class="la la-info-circle la-red"></i><span>Kelola setiap data museum dengan data terbaru</span></div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <table class="table-fix table-info table-hovered">
            <thead>
              <tr>
                <td>Nama event</td>
                <td width="200px">Konten</td>
                <td width="150px">Tanggal</td>
                <td>Jam</td>
                {{-- <td>Status</td> --}}
                <td>Status</td>
                <td width="150px">Option</td>
              </tr>
            </thead>
            <tbody>
              @forelse($events as $event)
              <tr>
                <td>{{ $event->title }}</td>
                <td>{{ str_limit($event->content, 80) }}</td>
                <td>{{ date('d M Y', strtotime($event->date)) }}</td>
                <td>{{ date('h:m', strtotime($event->time)) }}</td>
                {{-- <td>{{ $event->status }}</td> --}}
                @if($event->status != "published")
                <td>
                  {!! Form::model($event, ['url' => ['publish_event', $event], 'method' => 'patch', 'files' => true]) !!}
                    {!! Form::button('Publish event', ['type' => 'submit' ,'class' => 'btn btn-xs btn-warning', 'title'=>'Event belum dipublish']) !!}
                  {!! Form::close() !!}
                </td>
                @else
                <td>
                  {!! Form::model($event, ['url' => ['unpublish_event', $event], 'method' => 'patch', 'files' => true]) !!}
                    {!! Form::button('Unpublish event', ['type' => 'submit' ,'class' => 'btn btn-xs btn-danger', 'title'=>'Event telah dipublish']) !!}
                  {!! Form::close() !!}
                </td>
                @endif
                <td>
                  {!! Form::model($event, ['url' => ['delete_event', $event], 'method' => 'delete', 'class' => 'form-inline']) !!}
                  {{--  @if($event->status != "published")  --}}
                    <a href="{{ url('update_event', $event->id) }}"><i class="la la-pencil-square btn btn-xs btn-success" style="padding: 5px; font-size: 16px;"></i></a> 
                  {{--  @else
                    <a href="#"><i class="la la-edit btn btn-xs btn-default" style="padding: 5px; font-size: 16px;"></i></a> 
                  @endif  --}}
                  <a href="{{ url('show_event', $event->id) }}"><i class="la la-eye btn btn-xs btn-primary" style="padding: 5px; font-size: 16px;"></i></a>
                  {!! Form::button('<i class="la la-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xs btn-danger js-delete-confirm', 'style'=>'font-size: 16px;']) !!}
                  {!! Form::close() !!}
                </td>
              </tr>
              @empty
              <tr>
                <td colspan="7">Data event kosong</td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div> 
    {{ $events->links() }}
    @else
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="dash" style="margin-bottom: 20px;">
            <h1>Informasi Event</h1>
            <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12 text-center empty-state">
          <picture>
            <img src="{{ url('../img/empty02.png') }}" alt="">
            <p class="grey">Anda harus menambahkan informasi museum</p>
            <span class="grey">Silahkan tambahkan data terlebih dahulu untuk dapat melihat semua data</span>
            <a style="display:inline-block; margin-top:30px;" href="{{ url('add_museum') }}" class="btn-log">Tambahkan data museum</a>
          </picture>
        </div>
      </div>
    </div>
    @endif
  </div>

</div>

@endsection