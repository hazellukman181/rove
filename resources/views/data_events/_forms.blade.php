<div class="form-group {!! $errors->has('pamflet') ? 'has-error' : '' !!}">
	{!! Form::label('pamflet', 'Pamflet event') !!}
	@if(isset($model) && $model->pamflet !== '')
		<div class="row">
			<div class="col-md-6">
				{{-- <p>Current picture</p> --}}
				<div class="thumbnail">
					<img src="{{ url('/image/'.$model->pamflet) }}" class="img-rounded">
				</div>
			</div>
		</div>
	@endif
	{!! Form::file('pamflet', ['title'=>'Upload display gambar harus file gambar']) !!}
	<small>Upload pamflet event museum</small>
	{!! $errors->first('pamflet', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
	{!! Form::label('title', 'Nama event') !!}
	{!! Form::text('title', null, ['class'=>'form-control']) !!}
	<small>Isi dengan judul event</small>
	{!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('subtitle') ? 'has-error' : '' !!}">
	{!! Form::label('subtitle', 'Tema acara') !!}
	{!! Form::text('subtitle', null, ['class'=>'form-control']) !!}
	<small>Isi dengan tema acara dari event yang diadakan</small>
	{!! $errors->first('subtitle', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('content') ? 'has-error' : '' !!}">
	{!! Form::label('content', 'Deskripsi event') !!}
	{!! Form::textarea('content', null, ['class'=>'form-control']) !!}
	<small>Isi dengan deskripsi lengkap dari event</small>
	{!! $errors->first('content', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('date') ? 'has-error' : '' !!}" style="width: 300px;">
	{!! Form::label('date', 'Tanggal event') !!}
	{!! Form::text('date', null, ['class'=>'form-control input-date2', 'placeholder'=>'YYYY-DD-MM']) !!}
	<small>Isi dengan tanggal penyelenggaraan event</small>
	{!! $errors->first('date', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('time') ? 'has-error' : '' !!}" style="width: 300px;">
	{!! Form::label('time', 'Waktu pelaksanaan') !!}
	{!! Form::text('time', null, ['class'=>'form-control input-time', 'placeholder'=>'HH:MM']) !!}
	<small>Isi dengan waktu penyelenggaraan event</small>
	{!! $errors->first('time', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('cp') ? 'has-error' : '' !!}" style="width: 300px;">
	{!! Form::label('cp', 'Narahubung event') !!}
	{!! Form::text('cp', null, ['class'=>'form-control']) !!}
	<small>Isi dengan contact person dari panitia event</small>
	{!! $errors->first('cp', '<p class="help-block">:message</p>') !!}
</div>

{!! Form::hidden('museum_id', $id_museum) !!}
@if(isset($model))
{!! Form::hidden('status') !!}
@else
{!! Form::hidden('status', 'unpublish') !!}
@endif
<hr>
{!! Form::submit(isset($model) ? 'Perbarui data' : 'Simpan', ['class'=>'btn-log'] ) !!}
<a href="{{ url('event') }}" class="btn-log-back">Kembali</a>