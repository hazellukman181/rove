<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{{ url('/img/rove_logo.png') }}}">
    <title>Rove</title>

    {{-- <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet"> --}}
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body id="app-layout">
<div class="show-sm">
  <img src="{{ url('../img/logo_admin.png') }}" alt="">
  <h1>OUR MOBILE SITE COMING SOON! </h1>
  <p>FOR NOW ROVE ONLY AVAILABLE ON DESKTOP :)</p>
</div>
<div class="hidden-xs hidden-sm hidden-md">
<div class="container-fluid background">
  <div class="side-margin">
    <div class="row">
      <div class="navigation">
        <div class="col-md-6">
          <a href="{{ url('homepage') }}" class="container-logo">
            <img src="{{ url('../img/main_logo2.svg') }}" class="logo" alt="">
          </a>
        </div>
        <div class="col-md-6">
          @if(Auth::check())
            @can('pengelola-access')
              <div class="right-nav">
                <a href="{{ url('daftar_museum') }}">Museum</a>
                <a href="{{ url('daftar_event') }}">Event museum</a>
                <a href="{{url('/home')}}">{{ Auth::user()->name }}</a>
              </div>
            @endcan
          @endif

          @if(Auth::guest())
            <div class="right-nav">
              <a href="{{ url('daftar_museum') }}">Museum</a>
              <a href="{{ url('daftar_event') }}" class="{{ url('daftar_event ') == request()->url() ? 'orange' : '' }}">Event museum</a>
              <a href="{{url('auth/register/pengelola')}}">Register</a>
              <a href="{{ url('auth/login') }}" data-toggle="modal" data-target="#login">Login</a>
            </div>
          @endif
        </div>
      </div>
    </div>

    <div class="row">
      <div class="inner-text">
        <div class="col-md-6">
          <span><img class="elips" src="{{ url('../img/elipsis.svg') }}" alt="">Temukan cerita dibalik setiap koleksi museum</span>
          <h1 class="black">Temukan Cerita Baru di Setiap Museum</h1>
          <p class="grey">Dapatkan pengalaman baru dalam menjelajahi museum dengan perangkat androidmu</p>
          <a href="{{url('https://www.youtube.com/watch?v=ugIsJaTFj1c')}}" target="_blank" class="download">Video Aplikasi Rove</a>
          {{-- <span style="display: block; position: relative; top: 25px; font-size: 12px; opacity: .5;">Only for android device</span> --}}
          {{-- <img src="{{ url('../img/typekit.png') }}" alt="" class="tagline"> --}}
        </div>
      </div>
      
      <div class="col-md-6 text-right">
        <img class="droid" src="{{ url('../img/jumbo.png') }}" alt="">
      </div>
    </div>

  </div>
</div>

<div class="container-fluid">
  <div class="side-margin">
    <div class="row" style="margin-top: 100px;">
      <div class="col-md-5 middle-title">
        <h1>Jelajahi Semua Museum di Indonesia</h1>
        <h4>Aplikasi Rove mempunyai fitur menarik yang dapat memudahkan pengguna dalam mencari informasi setiap objek museum di seluruh indonesia</h4>
        <img class="trans1" src="{{ url('../img/trans_one.png') }}" alt="">
      </div>
    </div>
    <div class="row" style="margin-top: 40px; margin-bottom: 120px;">
      <div class="col-md-4">
        <div class="box-outter">
          <img class="image-fitur" src="{{ url('../img/fitur_one.svg') }}" alt="">
          <div class="fitur-title">
            Discover
          </div>
          <p class="grey">Temukan objek museum kemudian gunakan fitur scan QRCode pada aplikasi Rove</p>
        </div>
        <img class="trans2" src="{{ url('../img/trans_two.png') }}" alt="">
      </div>
      <div class="col-md-4">
        <div class="box-outter">
          <img class="image-fitur" src="{{ url('../img/fitur_two.svg') }}" alt="">
          <div class="fitur-title">
            Informasi
          </div>
          <p class="grey">Dapatkan berbagai informasi menarik mengenai event-event yang ada di museum</p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="box-outter">
          <img class="image-fitur" src="{{ url('../img/fitur_three.svg') }}" alt="">
          <div class="fitur-title">
            Temukan museum
          </div>
          <p class="grey">Temukan informasi lengkap dan akurat mengenai museum di seluruh indonesia</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="side-margin">
    <div class="row" style="margin-top: 100px;">
      <div class="col-md-5 middle-title">
        <h1>Cara Kerja Aplikasi Rove</h1>
        <h4>Cara kerja aplikasi Rove sangat mudah, lihat penjelasan mengenai cara penggunaan aplikasi Rove </h4>
        <img class="trans1" src="{{ url('../img/trans_one.png') }}" alt="">
      </div>
    </div>
    <div class="row" style="margin-top: 40px; margin-bottom: 120px;">
      <div class="col-md-6 text-center">
        <div class="video"></div>
        <video width="80%" src="{{ url('../img/sample.mp4') }}" type="video/mp4" autoplay loop></video>
      </div>
      <div class="col-md-6" style="margin-top: 50px;">
        <div class="panel-group" id="accordion1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title panel-custom">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapseOne"><i class="la la-search grey" style="font-size: 16px; margin-right: 5px;"></i>Jelajah Museum</a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <ul class="todo">
                        <li>
                          <div class="title">Scan QRCode</div>
                          <span class="grey">Scan QRCode yang terdapat di setiap koleksi museum</span>
                        </li>
                      </ul>
                    </div>
                    <div class="panel-body">
                      <ul class="todo">
                        <li>
                          <div class="title">Dapatkan Info Koleksi</div>
                          <span class="grey">Dapatkan informasi lengkap mengenai koleksi museum</span>
                        </li>
                      </ul>
                    </div>
                    <div class="panel-body">
                      <ul class="todo">
                        <li>
                          <div class="title">Bagikan Informasi</div>
                          <span class="grey">Bagikan infromasi yang didapat setelah melakukan proses scan QRCode</span>
                        </li>
                      </ul>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title panel-custom">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo"><i class="la la-bank grey" style="font-size: 16px; margin-right: 5px;"></i>Temukan Museum</a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                      <ul class="todo">
                        <li>
                          <div class="title">Cari Nama Museum</div>
                          <span class="grey">Lakukan pencarian dengan nama museum</span>
                        </li>
                      </ul>
                    </div>
                    <div class="panel-body">
                      <ul class="todo">
                        <li>
                          <div class="title">Dapatkan Hasil</div>
                          <span class="grey">Dapatkan hasil pencarian dan lihat semua informasi museum</span>
                        </li>
                      </ul>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title panel-custom">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapseThree"><i class="la la-calendar-check-o grey" style="font-size: 16px; margin-right: 5px;"></i>Informasi Event</a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                      <ul class="todo">
                        <li>
                          <div class="title">Lihat Daftar Event</div>
                          <span class="grey">Lihat semua daftar event yang diselenggarakan museum</span>
                        </li>
                      </ul>
                    </div>
                    <div class="panel-body">
                      <ul class="todo">
                        <li>
                          <div class="title">Dapatkan Info Event</div>
                          <span class="grey">Dapatkan informasi lengkap mengenai event</span>
                        </li>
                      </ul>
                    </div>
                    <div class="panel-body">
                      <ul class="todo">
                        <li>
                          <div class="title">Bagikan Event</div>
                          <span class="grey">Bagikan event melalui sosial media</span>
                        </li>
                      </ul>
                    </div>
                </div>
            </div>

        </div>

      </div>
    </div>
  </div>
</div>

<div class="container-fluid bg-gabung">
  <div class="side-margin">
    @if(Auth::check())
    <div class="row">
      <div class="col-md-6">
        <h1>Terima kasih telah bergabung bersama kami</h1>
        <a href="{{ url('home') }}">Kembali ke Dashboard</a>
      </div>
      <div class="col-md-6">
        <h4><i class="la la-info-circle" style="color: white; font-size: 30px; margin-right: 5px; position: relative; top: 4px;"></i>Mengapa harus bergabung?</h4>
        <p>Untuk anda pengelola museum, kami menyediakan platform yang dapat digunakan untuk
          mengelola semua informasi koleksi museum. Pengelola museum harus mendaftarkan diri dan
          menambahkan data koleksi museum ke web Rove untuk dapat menerapkan aplikasi Rove di museumnya 
        </p>
      </div>
    </div>
    @else
    <div class="row">
      <div class="col-md-6">
        <h1>Gabung Untuk Pengelola Museum</h1>
        <a href="{{ url('auth/register/pengelola') }}">Pendaftaran Pengelola</a>
      </div>
      <div class="col-md-6">
        <h4><i class="la la-info-circle" style="color: white; font-size: 30px; margin-right: 5px; position: relative; top: 4px;"></i>Mengapa harus bergabung?</h4>
        <p>Untuk anda pengelola museum, kami menyediakan platform yang dapat digunakan untuk
          mengelola semua informasi koleksi museum. Pengelola museum harus mendaftarkan diri dan
          menambahkan data koleksi museum ke web Rove untuk dapat menerapkan aplikasi Rove di museumnya 
        </p>
      </div>
    </div>
    @endif
  </div>
</div>

<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="padding: 20px 40px;">
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row footero">
    <div class="col-md-4">
      <img src="{{ url('../img/logo_full.png') }}" class="footer-logo" alt="">
    </div>
    <div class="col-md-8 footer-right">
      <a href="{{ url('daftar_museum') }}">Daftar museum</a>
      <a href="{{ url('daftar_event') }}">Event museum</a>
      <a href="{{ url('auth/register/pengelola') }}">Pendaftaran pengelola</a>
    </div>
  </div>
</div>
<div class="footer">
  <div class="text">
    <p class="footer-text">Dikembangkan dengan <i class="la la-heart" style="color: deeppink"></i> dari Semarang untuk museum di indonesia</p>
    <p class="text-right"><i class="la la-copyright"></i> ROVE. 2018 - All Rights Reserved</p>
  </div>
</div>
<div class="foot"></div>
</div>
    <script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>