<img class="koleksi-photo" src="{{ url('image/'.$koleksi->photo) }}" alt="">
<div class="content">
  <h4>{{ str_limit($koleksi->title, 45) }}</h4>
  <p>{{ str_limit($koleksi->story, 160) }}</p>
  <hr>
  <div class="footer-code">
    <img class="qrcode" src="{{ url('image/'.$koleksi->qrcode) }}" alt="">
    <div class="right-code">
			<small style="margin-top: 0px;">Kode koleksi museum</small>
      <h5>{{$koleksi->id_qrcode}}</h5>
    </div>
  </div>
  <div class="contorller">
    <a href="{{ url('show_koleksi/'.$koleksi->id) }}" class="btn btn-xs btn-success" style="display: inline-block;">Display</a>
    <a href="{{ url('update_koleksi/'.$koleksi->id.'/'.Auth::user()->id) }}" class="btn btn-xs btn-warning" style="display: inline-block;">Update</a>
    {!! Form::model($koleksi, ['url' => ['koleksi_delete', $koleksi], 'method' => 'delete', 'class' => 'form-inline', 'style'=>'display: inline-block;']) !!}
    {!! Form::button('Delete', ['type' => 'submit', 'class' => 'btn btn-xs btn-danger', 'style'=>'display: inline-block; padding: 5.5px 15px; font-size: 12px;']) !!}
    {!! Form::close() !!}
  </div>
</div>