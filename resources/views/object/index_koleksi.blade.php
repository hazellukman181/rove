@extends('layouts.app-admin')

@section('content')

<div class="container-fluid top-admin">
  @include('admin.top')
  
  <div class="row" style="margin-top: 30px;">
    @include('alert.alert')
    @if( isset($koleksis))
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-5">
          <div class="dash" style="margin-bottom: 20px;">
            <h1>Informasi Koleksi</h1>
            <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
          </div>
        </div>
        <div class="col-md-7 text-right" style="position: relative; top: 50px;">
          <a href="{{ url('print_all') }}" title="Cetak semua QRCode" class="btn-log" style="display: inline-block; padding: 7px 10px; font-size: 16px; position: relative; top: 1px;"><i class="la la-print"></i></a>
          <a href="add_koleksi/{{ Auth::user()->id }}" class="btn-log" style="display: inline-block; ">Tambah koleksi</a>
          {!! Form::open(['url'=>['koleksi'], 'method'=>'get', 'style'=>'width: 300px; display: inline-block']) !!}
            {!! Form::text('query', null, ['class'=>'form-control','style'=>'display: inline-block; width: 240px;']) !!}
            {!! Form::button('<i class="la la-search"></i>', ['type'=>'submit', 'class'=>'btn-log', 'style'=>'display: inline-block; padding: 5px 10px; font-size: 18px;']) !!}
          {!! Form::close() !!}
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="alert-red"><i class="la la-info-circle la-red"></i><span>Kelola setiap data museum dengan data terbaru</span></div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <table class="table-fix table-info table-hovered">
            <thead> 
              <tr>
                <td>Nama koleksi</td>
                {{--  <td>Museum</td>  --}}
                <td width="280px">QRCode</td>
                <td>Tanggal ditambahkan</td>
                <td width="140px;">Pilihan</td>
                <td width="180px;">Download QRCode</td>
              </tr>
            </thead>
            <tbody>
              @forelse($koleksis as $koleksi)
                <tr>
                  <td>{{ str_limit($koleksi->title, 45) }}</td>
                  {{--  <td>{{ $koleksi->museum->user->name }}</td>  --}}
                  <td>
                    <picture style="display:inline-block; vertical-align: top; width: 90px;">
                      <img class="qrcode" src="{{ url('image/'.$koleksi->qrcode) }}" alt="">
                    </picture>
                    <div class="kode">
                      <span class="grey">Kode koleksi</span>
                      <span style="font-size: 16px;"><b>{{ $koleksi->id_qrcode }}</b></span>
                    </div>
                  </td>
                  <td>{{ date('d F Y' , strtotime($koleksi->created_at)) }}</td>
                  <td>
                    {{--  <a href="{{ url('print/'.$koleksi->id) }}" class="btn btn-xs btn-primary" style="display: inline-block; padding: 2px 5px;"><i class="la la-print"></i></a>  --}}
                    <a href="{{ url('show_koleksi/'.$koleksi->id) }}" class="btn btn-xs btn-success" style="display: inline-block; padding: 2px 5px;"><i class="la la-eye"></i></a>
                    <a href="{{ url('update_koleksi/'.$koleksi->id.'/'.Auth::user()->id) }}" class="btn btn-xs btn-warning" style="display: inline-block; padding: 2px 5px;"><i class="la la-pencil"></i></a>
                    {!! Form::model($koleksi, ['url' => ['koleksi_delete', $koleksi], 'method' => 'delete', 'class' => 'form-inline', 'style'=>'display: inline-block;']) !!}
                    {!! Form::button('<i class="la la-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-xss btn-danger', 'style'=>'display: inline-block; padding: 2.5px 5px; font-size: 12px;']) !!}
                    {!! Form::close() !!}
                  </td>
                  <td>
                    <a href="{{ url('print/'.$koleksi->id) }}" class="btn btn-sm btn-default" title="Download masing-masing QRCode">Download</a>
                    {{--  <a href="{{ url('download_qrcode/'.$koleksi->id) }}" class="btn btn-sm btn-default" style="font-size: 14px;">Download</a>  --}}
                  </td>
                </tr>
              @empty
                <tr>
                  @if(isset($q))
                  <td colspan="5">Data koleksi <b>"{{ $q }}"</b> tidak ditemukan</td>
                  @else
                  <td colspan="5">Data koleksi masih kosong, Silahkan tambahkan koleksi</td>
                  @endif
                </tr>
              @endforelse
              </tbody>
            </table>
	          	{{-- @include('object.display_content') --}}
        </div>  
      </div>
    </div>
    @else
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="dash" style="margin-bottom: 20px;">
            <h1>Informasi Koleksi</h1>
            <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12 text-center empty-state">
          <picture>
            <img src="{{ url('../img/empty02.png') }}" alt="">
            <p class="grey">Anda harus menambahkan informasi museum</p>
            <span class="grey">Silahkan tambahkan data terlebih dahulu untuk dapat melihat semua data</span>
            <a style="display:inline-block; margin-top:30px;" href="{{ url('add_museum') }}" class="btn-log">Tambahkan data museum</a>
          </picture>
        </div>
      </div>
    </div>
    @endif
  </div>

</div>

@endsection