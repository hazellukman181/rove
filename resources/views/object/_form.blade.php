<div class="form-group {!! $errors->has('photo') ? 'has-error' : '' !!}">
	{!! Form::label('photo', 'Foto koleksi') !!}
	@if(isset($model) && $model->photo !== '')
		<div class="row">
			<div class="col-md-12">
				{{-- <p>Current picture</p> --}}
				<div class="thumbnail">
					<img src="{{ url('/image/'.$model->photo) }}" class="img-rounded">
				</div>
			</div>
		</div>
	@endif
	{!! Form::file('photo', ['title'=>'Upload display gambar harus file gambar']) !!}
	<small>Upload photo koleksi museum</small>
	{!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
</div>

@if(isset($koleksi))
<div class="form-group {!! $errors->has('id_qrcode') ? 'has-error' : '' !!}">
	{!! Form::label('id_qrcode', 'Kode koleksi museum') !!}
	{!! Form::text('id_qrcode', null, ['class'=>'form-control', 'disabled'=>'disabled', 'title'=>'Kode koleksi tidak dapat diubah']) !!}
	<small>Isi dengan id koleksi</small>
	{!! $errors->first('id_qrcode', '<p class="help-block">:message</p>') !!}
</div>
@else
<div class="form-group {!! $errors->has('id_qrcode') ? 'has-error' : '' !!}">
	{!! Form::label('id_qrcode', 'Kode koleksi museum') !!}
	{!! Form::text('id_qrcode', null, ['class'=>'form-control']) !!}
	<small>Isi dengan id koleksi</small>
	{!! $errors->first('id_qrcode', '<p class="help-block">:message</p>') !!}
</div>
@endif

<div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
	{!! Form::label('title', 'Nama koleksi') !!}
	{!! Form::text('title', null, ['class'=>'form-control']) !!}
	<small>Isi dengan nama koleksi</small>
	{!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('story') ? 'has-error' : '' !!}">
	{!! Form::label('story', 'Deskripsi koleksi') !!}
	{!! Form::textarea('story', null, ['class'=>'form-control']) !!}
	<small>Isi dengan deskripsi koleksi museum</small>
	{!! $errors->first('story', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('story1') ? 'has-error' : '' !!}">
	{!! Form::label('story1', 'Deskripsi koleksi 1 (opsional)') !!}
	{!! Form::textarea('story1', null, ['class'=>'form-control']) !!}
	<small>Isi dengan deskripsi koleksi museum</small>
	{!! $errors->first('story1', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('story2') ? 'has-error' : '' !!}">
	{!! Form::label('story2', 'Deskripsi koleksi 2 (opsional)') !!}
	{!! Form::textarea('story2', null, ['class'=>'form-control']) !!}
	<small>Isi dengan deskripsi koleksi museum</small>
	{!! $errors->first('story2', '<p class="help-block">:message</p>') !!}
</div>

{!! Form::hidden('museum_id', $id_museum) !!}
<hr>
{!! Form::submit(isset($model) ? 'Perbarui data' : 'Simpan', ['class'=>'btn-log'] ) !!}
<a href="{{ url('koleksi') }}" class="btn-log-back">Kembali</a>
