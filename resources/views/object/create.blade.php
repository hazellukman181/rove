@extends('layouts.app-admin')

@section('content')

<div class="container-fluid top-admin">
  @include('admin.top')

  <div class="row"  style="margin-top: 30px;">
    <div class="col-md-">
      <div class="dash" style="margin-bottom: 20px;">
        <span><a href="{{ url('koleksi') }}">Informasi Koleksi</a></span> <i class="la la-angle-right"></i> <span>Tambah Data Koleksi</span>
        <hr>
        <h1>Tambah Koleksi</h1>
        <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
      </div> 
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="alert-red"><i class="la la-info-circle la-red"></i><span>Kelola setiap data museum dengan data terbaru</span></div>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-md-8">
      {!! Form::open(['url' => ['koleksi_add'], 'method' => 'post', 'files' => true]) !!}
        @include('object._form')
      {!! Form::close() !!}
    </div>
  </div>

</div>

@endsection