@extends('layouts.app-admin')

@section('content')

<div class="container-fluid top-admin">
  @include('admin.top')

  <div class="row" style="margin-top: 30px;">
    @include('alert.alert')
    @if(isset($museum))
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="dash" style="margin-bottom: 20px;">
            <h1>Informasi Museum</h1>
            <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
          </div>
        </div>
        <div class="col-md-6 text-right">
          <a href="museum/{{ Auth::user()->id }}" class="btn-log" style="position: relative; top: 70px; right: 0px;">Perbarui Informasi</a>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="alert-red"><i class="la la-info-circle la-red"></i><span>Kelola setiap data museum dengan data terbaru</span></div>
        </div>
      </div>
      <hr>
      {{-- {{ dd($museum) }} --}}
      <div class="row">
        <div class="col-md-12">
          <picture class="image-museum">
            <img src="{{ url('../image/'.$museum->photo) }}" alt="">
            <div class="museum-cover">
              <div class="museum-name">{{ $museum->name }}</div>
              <div class="museum-city">{{ $museum->city }}</div>
            </div>
          </picture>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <table class="table-fix table-info table-hovered">
            <thead>
              <tr>
                <td width='200px'>Informasi</td>
                <td>Konten data</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Nama museum</td>
                <td>{{ $museum->name }}</td>
              </tr>
              <tr>
                <td>Alamat museum</td>
                <td>{{ $museum->address }}</td>
              </tr>
              <tr>
                <td>Nomor telepon museum</td>
                <td>{{ $museum->phone }}</td>
              </tr>
              <tr>
                <td>Deskripsi museum</td>
                <td>{{ $museum->detail }}</td>
              </tr>
              <tr>
                <td>Provinsi</td>
                <td>{{ $museum->province }}</td>
              </tr>
              <tr>
                <td>Jam operasional</td>
                <td>{{ $museum->hours }} WIB</td>
              </tr>
              <tr>
                <td>Harga tiket</td>
                <td>Rp {{ $museum->price }}</td>
              </tr>
          </tbody>
          </table>
        </div>
      </div>
    </div>
    @else
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="dash" style="margin-bottom: 20px;">
            <h1>Informasi Museum</h1>
            <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
          </div>
        </div>
        <div class="col-md-6 text-right">
          <a href="add_museum" class="btn-log" style="position: relative; top: 70px; right: 0px;">Tambah Data</a>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12 text-center empty-state">
          <picture>
            <img src="{{ url('../img/empty02.png') }}" alt="">
            <p class="grey">Data ini masih kosong</p>
            <span class="grey">Silahkan tambahkan data terlebih dahulu untuk dapat melihat semua data</span>
          </picture>
        </div>
      </div>
    </div>
    @endif
  </div>
</div>

@endsection