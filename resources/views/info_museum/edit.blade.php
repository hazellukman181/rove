@extends('layouts.app-admin')

@section('content')

<div class="container-fluid top-admin">
  @include('admin.top')

  <div class="row" style="margin-top: 30px;">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <div class="dash" style="margin-bottom: 20px;">
            <span><a href="{{ url('museum') }}">Daftar museum</a></span> <i class="la la-angle-right"></i> <span>Update Informasi Museum</span>
            <hr>
            <h1>Update Informasi Museum</h1>
            <p style="width: 100% !important" class="grey">Pengelolaan data museum dan data koleksi museum</p>
          </div>
        </div>
      </div>
      {{-- {{ dd($museum) }} --}}
      <div class="row">
        @if(isset($museum))
        <div class="col-md-12">
          <picture class="image-museum">
            <img src="{{ url('../image/'.$museum->photo) }}" alt="">
            <div class="museum-cover">
              <div class="museum-name">{{ $museum->name }}</div>
              <div class="museum-city">{{ $museum->city }}</div>
            </div>
          </picture>
        </div>
        @endif
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="alert-red"><i class="la la-info-circle la-red"></i><span>Kelola setiap data museum dengan data terbaru</span></div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-8">
          {!! Form::model($museum, ['url' => ['museum_update', $museum], 'method' => 'patch', 'files' => true]) !!}
            @include('info_museum._form', ['model' => $museum])
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
