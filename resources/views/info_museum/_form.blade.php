<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
	{!! Form::label('name', 'Nama museum') !!}
	{!! Form::text('name', null, ['class'=>'form-control']) !!}
	<small>Isi dengan data mengenai nama museum</small>
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('detail') ? 'has-error' : '' !!}">
	{!! Form::label('detail', 'Deskripsi museum') !!}
	{!! Form::textarea('detail', null, ['class'=>'form-control']) !!}
	<small>Isi dengan data deskripsi yang menjelaskan tentang koleksi museum</small>
	{!! $errors->first('detail', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('address') ? 'has-error' : '' !!}">
	{!! Form::label('address', 'Alamat museum') !!}
	{!! Form::text('address', null, ['class'=>'form-control']) !!}
	<small>Isi dengan data mengenai alamat museum</small>
	{!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('city') ? 'has-error' : '' !!}" style="width: 300px;">
	{!! Form::label('city', 'Kota') !!}
	{!! Form::text('city', null, ['class'=>'form-control']) !!}
	<small>Isi dengan kota lokasi museum</small>
	{!! $errors->first('city', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('province') ? 'has-error' : '' !!}" style="width: 300px;">
	{!! Form::label('province', 'Provinsi') !!}
	{!! Form::text('province', null, ['class'=>'form-control']) !!}
	<small>Isi dengan provinsi lokasi museum</small>
	{!! $errors->first('province', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('phone') ? 'has-error' : '' !!}" style="width: 300px;">
	{!! Form::label('phone', 'Nomor telepon museum') !!}
	{!! Form::text('phone', null, ['class'=>'form-control input-phone', 'maxlength'=>14]) !!}
	<small>Isi dengan data mengenai nomor telepon museum</small>
	{!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('hours') ? 'has-error' : '' !!}" style="width: 300px;">
	{!! Form::label('hours', 'Jam operasional') !!}
	{!! Form::text('hours', null, ['class'=>'form-control input-hours', 'placeholder'=>'hh:mm-hh:mm']) !!}
	<small>Isi dengan data mengenai jam operasional museum</small>
	{!! $errors->first('hours', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('price') ? 'has-error' : '' !!}" style="width: 300px;">
	{!! Form::label('price', 'Harga tiket masuk') !!}
	{!! Form::text('price', null, ['class'=>'form-control input-currency']) !!}
	<small>Isi dengan data mengenai harga tiket museum</small>
	{!! $errors->first('price', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('photo') ? 'has-error' : '' !!}">
	{!! Form::label('photo', 'Foto museum') !!}
	{!! Form::file('photo', ['title'=>'Upload display gambar harus file gambar']) !!}
	<small>Ketentuan foto (format: jpg, png, jpeg & size foto tidak lebih dari 1mb </small>
	{!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
</div>
<hr>
{!! Form::submit(isset($model) ? 'Perbarui data' : 'Simpan', ['class'=>'btn-log'] ) !!}

{!! Form::hidden('user_id', Auth::user()->id) !!}
<a href="{{ url('museum') }}" class="btn-log-back">Kembali</a>
