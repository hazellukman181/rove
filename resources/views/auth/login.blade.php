@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="side-margin">
    <div class="regis-panel">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1 class="bingo">Kelola objek museum</h1>
        <p class="bingo2 grey">Pengelola museum dapat melakukan pendaftaran ke sistem Rove untuk mengelola data mengenai semua objek museum agar dapat di sinkronisasi pada android</p>
      </div>
    </div>
    
    <div class="row">
      <div class="panel-form2 col-md-6 col-md-offset-3">
        <hr>
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
          {{ csrf_field() }}

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
              <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username">

              @if ($errors->has('username'))
                <span class="help-block">
                  {{ $errors->first('username') }}
                </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <div class="addon">
                <input id="password" type="password" class="form-control" name="password" placeholder="Password">
                <span toggle="#password" class="la la-eye toggle-password"></span>
                @if ($errors->has('password'))
                  <span class="help-block">
                    {{ $errors->first('password') }}
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group">
              <button type="submit" class="btn-log">
                Masuk
              </button>
              <p style="font-size: 12px; text-align: center; margin-top: 20px;">Apabila belum mempunyai akun silahkan <a href="{{ url('auth/register/pengelola') }}">Register</a></p>
                {{-- <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a> --}}
            </div>
          </form>
      </div>
    </div>
    </div>

  </div>
</div>
@endsection
