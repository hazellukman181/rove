@extends('layouts.app')

@section('content')

<div class="container-fluid background-museum">
  <div class="side-margin">
    
    <div class="regis-panel">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1 class="bingo">Daftar untuk mengelola koleksi museum</h1>
        <p class="bingo2 grey">Dengan mendaftar pengelola museum dapat menambahkan koleksi museum kedalam sistem arag dapat di akses melalui aplikasi android</p>
      </div>
    </div>
    
    <div class="row">
      <div class="panel-form2 col-md-6 col-md-offset-3">
        <hr>
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}

          <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username">

            @if ($errors->has('username'))
              <span class="help-block">
                {{ $errors->first('username') }}
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nama lengkap">

            @if ($errors->has('name'))
              <span class="help-block">
                {{ $errors->first('name') }}
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Alamat email">

            @if ($errors->has('email'))
              <span class="help-block">
                {{ $errors->first('email') }}
              </span>
            @endif
          </div>

          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="addon">
              <input id="password" type="password" class="form-control" name="password" placeholder="Password">
              <span toggle="#password" class="la la-eye toggle-password"></span>
              @if ($errors->has('password'))
                <span class="help-block">
                  {{ $errors->first('password') }}
                </span>
              @endif
            </div>
          </div>

          <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="Ulangi password">
            @if ($errors->has('password_confirmation'))
              <span class="help-block">
                {{ $errors->first('password_confirmation') }}
              </span>
            @endif
          </div>

          <div class="form-group">
            <button type="submit" class="btn-log">
              Daftar sekarang
            </button>
            <p style="font-size: 12px; text-align: center; margin-top: 20px;">Apabila sudah mempunyai akun silahkan <a href="{{ url('auth/login/pengelola') }}">Login</a></p>
          </div>
        </form>
      </div>
    </div>
    </div>

  </div>
</div>

@endsection
