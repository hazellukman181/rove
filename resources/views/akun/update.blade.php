@extends('layouts.app-admin')

@section('content')

<div class="container-fluid top-admin">
  @include('admin.top')

  <div class="row" style="margin-top: 30px;">
    @include('alert.alert')
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="dash" style="margin-bottom: 20px;">
            {{--  <div class="timber">  --}}
              <span><a href="{{ url('home') }}">Dashboard</a></span> <i class="la la-angle-right"></i> <span>Update Informasi Akun</span>
            {{--  </div>  --}}
            <hr>
            <h1>Informasi Akun</h1>
            <p style="width: 100% !important" class="grey">Pengelolaan data akun pengelola museum</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="alert-red"><i class="la la-info-circle la-red"></i><span>Kelola setiap data museum dengan data terbaru</span></div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-6">
          {{-- {{ dd($user) }} --}}
          {!! Form::model($user, ['url' => ['update_akun', $user], 'method' => 'patch']) !!}
            @include('akun._form')
          {!! Form::close() !!}
        </div>
      </div>

    </div>
  </div>
</div>

@endsection