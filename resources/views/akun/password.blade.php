@extends('layouts.app-admin')

@section('content')

<div class="container-fluid top-admin">
  @include('admin.top')

  <div class="row" style="margin-top: 30px;">
    @include('alert.alert')
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="dash" style="margin-bottom: 20px;">
            {{--  <div class="timber">  --}}
              <span><a href="{{ url('home') }}">Dashboard</a></span> <i class="la la-angle-right"></i> <span>Update Password</span>
            {{--  </div>  --}}
            <hr>
            <h1>Password</h1>
            <p style="width: 100% !important" class="grey">Perbarui password anda agar keamanan akun terjaga</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="alert-red"><i class="la la-info-circle la-red"></i><span>Kelola setiap data museum dengan data terbaru</span></div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-8">
          <form role="form" method="POST" action="{{ url('/update_password/'.$user->id) }}">
          <input type="hidden" name="_method" value="PATCH">
            {{ csrf_field() }}
          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="addon">
              <label for="password">Password baru</label>
              <input id="password" type="password" class="form-control" name="password">
              <span toggle="#password" class="la la-eye toggle-password" style="font-size: 25px;position: absolute;top: 32px;right: 20px;"></span>
              @if ($errors->has('password'))
                <span class="help-block">
                  {{ $errors->first('password') }}
                </span>
              @endif
              <small style="margin-bottom: 15px;">Masukkan password baru</small>
            </div>
          </div>

          <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="password">Ulangi password</label>
            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation">
            @if ($errors->has('password_confirmation'))
              <span class="help-block">
                {{ $errors->first('password_confirmation') }}
              </span>
            @endif
            <small>Ketik ulang password baru anda</small>
          </div>

          <div class="form-group">
            <hr>
            <button type="submit" class="btn-log">
              Perbarui data
            </button>
            <a href="{{ url('home') }}" class="btn-log-back">Kembali</a>
          </div>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>

@endsection