<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
	{!! Form::label('name', 'Nama lengkap') !!}
	{!! Form::text('name', null, ['class'=>'form-control']) !!}
	<small>Isi dengan nama lengkap pengelola</small>
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('username') ? 'has-error' : '' !!}">
	{!! Form::label('username', 'Username') !!}
	{!! Form::text('username', null, ['class'=>'form-control']) !!}
	<small>Isi dengan username baru</small>
	{!! $errors->first('username', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
	{!! Form::label('email', 'Alamat email') !!}
	{!! Form::text('email', null, ['class'=>'form-control', 'disabled'=>'disabled', 'title'=>'Alamat email hanya dapat diperbarui melalui reset password']) !!}
	<small>Isi dengan data email</small>
	{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<hr>
{!! Form::submit('Perbarui data', ['class'=>'btn-log']) !!}
<a href="{{ url('home') }}" class="btn-log-back">Kembali</a>