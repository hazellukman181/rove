<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Cetak QRCode</title>
		{{-- <link rel="stylesheet" href="{{ url('css/export.css') }}"> --}}
	</head>
	<body>
		<style>
			table{
				border-collapse: collapse; 
			} 
			.table td{
				border: 0.5px solid #000000; font-size: 12px; padding: 5px 7px; text-align: center;
			}
			.tablo{ 
				table-layout: fixed;
				width: 220px;
				margin-bottom: 10px;
				font-size: 14px;
			}
			.tablo tr td{
        border: 1px solid #000; font-weight: normal; text-align: left;
			}
			.center{
				text-align: center !important;
      }
      .disc {
        margin-top: 5px;
        font-size: 10px;
        text-align: center;
        color: #868686;
      }
      .image {
        padding: 10px 5px;
      }
      .zuma {
        width: 100%;
			}
			.took {
				position: relative;
				width: 500px;
				box-sizing: border-box;
			}
			.took .container{
				text-align: center;
				vertical-align: top;
				width: 40%;
				float: left;
				position: relative;
				display: block;
				border: 1px solid #000000;
				box-sizing: border-box;
			}
			.took .container img {
				padding-top: 10px;
				text-align: center;
				width: 80%;
				display: block;
			}
			.took .container span {
				display: block;
				border-top: 1px solid #000;
			}
		</style>
		{{--  {{ dd($data_museum) }}  --}}
		<h3>Daftar QR Code {{ $data_museum->name }}</h3>
		<p>Oleh: {{ $data_museum->user->name }} | Didownload pada: {{ $now }}</p>
		<hr>
		<br>
		<br>
		<br>
		@foreach($data as $object)
		{{--  <div class="span" style="border: 1px solid #000; margin-right: 10px;">  --}}
			<img style="border: 1px solid #000; margin-right: 10px; margin-bottom: 50px;" src="{{ public_path() . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . $object->qrcode }}" alt="">
			<div class="din" style="position: relative; display: inline-block;">
				<p style="display: inline-block; position:absolute; left: -210px;">{{ $object->id_qrcode }}</p>
			</div>
		{{--  </div>  --}}
		{{--  <div class="top" style="position: relative; float: left;">
			<span style="position: absolute;">{{ $object->title }}</span>
		</div>  --}}
		{{--  <table class="tablo">
      <thead>
        <tr>
          <td colspan="2" class="center image"><img class="zuma" src="{{ public_path() . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . $object->qrcode }}" alt=""></td>
        </tr>
        <tr>
          <td>Nama koleksi</td>
          <td style="width:420px; word-break:break-all; word-wrap:break-word;"><span style="width:120px; word-break:break-all; word-wrap:break-word;">{{ $object->title }}</span></td>
        </tr>
        <tr>
          <td>Kode koleksi</td>
          <td style="width:420px; word-break:break-all; word-wrap:break-word;">{{ $object->id_qrcode }}</td>
        </tr>
      </thead>
		</table>  --}}
		{{--  <div class="took">
			<div class="container">
				<img class="zuma" src="{{ public_path() . DIRECTORY_SEPARATOR . 'image' . DIRECTORY_SEPARATOR . $object->qrcode }}" alt="">
				<span>{{ $object->title }}</span>
				<span>{{ $object->id_qrcode }}</span>
			</div>
		</div>  --}}
		@endforeach
	</body>
</html>