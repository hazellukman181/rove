$('div.alert').delay(5000).slideUp(300);

for (let field of $('.input-date').toArray()) {
  new Cleave(field, {
    date: true,
    datePatern: ['d', 'm', 'Y'],
    delimiter: '-'
  });
}


for (let field of $('.input-time').toArray()) {
  new Cleave(field, {
    blocks: [2, 2],
    delimiter: ':'
  });
}

for (let field of $('.input-hours').toArray()) {
  new Cleave(field, {
    blocks: [2, 2, 2, 2],
    delimiters: [':', '-', ':']
  });
}

for (let field of $('.input-date2').toArray()) {
  new Cleave(field, {
    date: true,
    datePattern: ['Y', 'm', 'd'],
    delimiter: '-'
  });
}

for (let field of $('.input-currency').toArray()) {
  new Cleave(field, {
    numeral: true,
    numeralThousandsGroupStyle: 'thousand'
  });
}

$(".toggle-password").click(function () {

  $(this).toggleClass("la-eye la-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
